export default {
  items: [
    {
      name: 'Orders Managment',
      url: '/DistHistory',
      icon: 'fa fa-home',
    },
    {
      name: 'My Products',
      url: '/DistProducts',
      icon: 'icon-basket',
    }
  ],
};
