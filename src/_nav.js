export default {
  items: [
    {
      name: 'Home',
      url: '/dashboard',
      icon: 'fa fa-home',
    },
    {
      name: 'Orders Management',
      url: '/OrderHistory',
      icon: 'icon-list',
    },
    {
      name: 'Market',
      url: '/Products',
      icon: 'icon-basket',
    }
  ],
};
