import React, { Component } from 'react';
import { TabContent, TabPane, Modal, ModalHeader, ModalBody, ModalFooter, Button } from 'reactstrap';
import PropTypes from 'prop-types';
// import classNames from 'classnames';
import rp from 'request-promise';
import Cookies from 'universal-cookie';
// eslint-disable-next-line
import { ToastContainer, toast } from 'react-toastify';

const cookies = new Cookies();

const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};


var token = '';




class DefaultAside extends Component {

  constructor(props) {
    super(props);

    this.toggleSuccess = this.toggleSuccess.bind(this);
    this.storageListener = this.storageListener.bind(this);
    this.updateOrderHistory = this.updateOrderHistory.bind(this);
    this.clearBasket = this.clearBasket.bind(this);
    this.checkout = this.checkout.bind((this));
    this.notification = this.notification.bind(this);
    //dummy data
    if (localStorage.getItem("data") === null) {
      localStorage.setItem("data", JSON.stringify([]))
    }

    this.data = JSON.parse(localStorage.getItem("data"));

    var basket_total = 0;

    for (var i = 0; i < this.data.length; i++) {
      var basket_item = this.data[i];
      basket_total += basket_item.quantity * basket_item.pPrice;
    }

    this.toggle = this.toggle.bind(this);
    this.state = {
      activeTab: '4',
      basket_data: this.data,
      basket_total: basket_total,
      success: false,
    };



  }

  componentWillMount() {
    if (localStorage.getItem("id_token")) {
      token = localStorage.getItem("id_token")
    }

    if (cookies.get('id_token')) {
      token = cookies.get('id_token')
    }
  }



  getBasketTotal() {
    var basket_total = 0;

    for (var i = 0; i < this.state.basket_data.length; i++) {
      var basket_item = this.state.basket_data[i];
      basket_total += basket_item.quantity * basket_item.pPrice;
    }

    this.setState({
      basket_total: basket_total
    });
    localStorage.setItem('data', JSON.stringify(this.state.basket_data))
  }

  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab,
      });
    }

  }

  decQuant = (key) => {
    let arrayCopy = [...this.state.basket_data];

    for (var i = 0; i < arrayCopy.length; i++) {
      if (arrayCopy[i].pID === key) {
        arrayCopy[i].quantity -= 1
        break
      }
    }
    this.setState({ basket_data: arrayCopy });
    localStorage.setItem("data", JSON.stringify(arrayCopy))
    this.getBasketTotal();

  }

  incQuant = (key) => {
    let arrayCopy = [...this.state.basket_data];

    for (var i = 0; i < arrayCopy.length; i++) {
      if (arrayCopy[i].pID === key) {
        arrayCopy[i].quantity += 1
        break
      }
    }
    this.setState({ basket_data: arrayCopy });
    localStorage.setItem("data", JSON.stringify(arrayCopy))

    this.getBasketTotal();

  }

  deleteItem = (key) => {
    let arrayCopy = [...this.state.basket_data];


    for (var i = 0; i < arrayCopy.length; i++) {
      if (arrayCopy[i].pID === key) {
        key = i
        break
      }
    }

    arrayCopy.splice(key, 1);
    this.setState({ basket_data: arrayCopy });

    if (arrayCopy.length === 0) {
      localStorage.removeItem("data")
      localStorage.setItem("data", JSON.stringify([]))
    }

    localStorage.setItem("data", JSON.stringify(arrayCopy))
    // eslint-disable-next-line
    this.state.basket_data = arrayCopy;
    this.getBasketTotal();

    var event = new Event('itemDeleted');
    window.dispatchEvent(event);
  }

  toggleSuccess() {
    this.setState({
      success: !this.state.success,
    });



  }

  clearBasket() {
    localStorage.removeItem("data");
    localStorage.setItem("data", JSON.stringify([]));
    var event = new Event('basketDeleted');
    window.dispatchEvent(event);
    this.setState({ basket_data: [], basket_total: 0 })
  }

  storageListener = () => {
    var that = this;
    window.addEventListener("itemInserted", function () {
      that.setState({ basket_data: JSON.parse(localStorage.getItem("data")) })
      that.state.basket_data = JSON.parse(localStorage.getItem("data"))
      that.getBasketTotal()
    }, false)
  }


  basketByDist = () => {
    var groupByDist = []
    let arrayCopy = [...this.state.basket_data];


    // eslint-disable-next-line
    Array.prototype.containsDist = function (elem) {
      for (var i in this) {
        if (this[i].pDID === elem) return true;
      }
      return false;
    }

    for (let product of arrayCopy) {
      if (!groupByDist.containsDist(product.pDID)) {
        var new_value = {
          pDID: product.pDID,
          uFname: product.uFname,
          data: [product]
        }
        groupByDist.push(new_value)
      }
      else {
        groupByDist.filter(function (d) {
          var prod_found = false
          if (d.pDID === product.pDID) {
            d.data.filter(function (info) {
              if (info.pID === product.pID) {
                info.quantity += 1
                prod_found = true
              }

              return null
            });
            if (!prod_found) {
              d.data.push(product)
            }
          }
          return null
        });
      }
    }

    return groupByDist
  }

  checkout() {
    rp(this.updateOrderHistory()).then(this.notification())
    
  }

  notification() {
    console.log("kokos");
    toast.success("Thank you for your order! Check order management for futrher details.", {
      position: toast.POSITION.BOTTOM_CENTER,
      autoClose: 4000
    })

  }


  updateOrderHistory() {
    this.toggleSuccess();
    var ord = this.basketByDist();
    let ohTotal;

    let user_details = [];

    var udDetails = {
      uri: "https://sng-heroku-api.herokuapp.com/api/user_details",
          headers: {
            Authorization: 'Bearer ' + token
          },
          method: 'GET',
          json: true
    }
    rp(udDetails).then( 
      (udDe) => {

        user_details = udDe.data[0]

        this.setState({
          user_details: user_details
        })
    },

    ord.forEach(
      (e) => {
        let ohDID = e.data[0].pDID;
        ohTotal = 0;
        e.data.forEach((o) => {
          ohTotal += (o.pPrice * o.quantity);
        })

        var sentOrder = {
          uri: "https://sng-heroku-api.herokuapp.com/api/orders",
          headers: {
            data: JSON.stringify([null, ohTotal.toFixed(2), null, ohDID]),
            Authorization: 'Bearer ' + token
          },
          method: 'POST',
          json: true
        }
        rp(sentOrder).then(

          () => {

            var latestOrderID = {
              uri: "https://sng-heroku-api.herokuapp.com/api/latest_order_id",
              headers: {
                data: JSON.stringify({ "ohDID": ohDID }),
                // data: `SELECT ohID FROM order_history where ohUID = ${id} and ohDID = ${ohDID} ORDER BY ohID DESC LIMIT 1;`,
                Authorization: 'Bearer ' + token
              },
              method: 'GET',
              json: true

            }

            rp(latestOrderID).then(

              (latestID) => {
                if (latestID.data === undefined) {
                  latestID.data.push({ ohID: 1 })
                }
                var latestid = latestID.data[0].ohID;
                var orderProducts = []
                e.data.forEach((o) => {
                  //orderProducts.push([{ "opOHID": latestid }, { opPID: o.pID }, { opQuant: o.quantity }, { opDisc: 0.00 }, { opPrice: o.pPrice }, { opName: o.pName }, { opDesc: o.pDesc }, { opVat: 0.19 }])
                  orderProducts.push([latestid, o.pID, o.quantity, 0.00, o.pPrice, o.pName, o.pDesc, o.pVAT])
                })


                var order_products = {

                  uri: "https://sng-heroku-api.herokuapp.com/api/order_products",
                  headers: {
                    data: JSON.stringify(orderProducts),
                    Authorization: 'Bearer ' + token
                  },
                  method: 'POST',
                  json: true

                }
                rp(order_products).then(
                  (res) => {
                    return res
                  }).catch((error) => {
                    console.log(error)
                  })
                return true;

              }).catch((error) => {
                console.log(error)
              })
            // eslint-disable-next-line
            let distEmail = e.data[0].udEmail;
            var email = {
              uri: "https://sng-heroku-api.herokuapp.com/api/send_email",
              headers: {
                data: JSON.stringify({order: e, distEmail: distEmail, sendingOrder: true, details: user_details}),
                //data: JSON.stringify({ order: e, sendingOrder: true }),
                Authorization: 'Bearer ' + token
              },
              method: 'POST',
              json: true
            }
            rp(email).then(
              (em) => {
                return em;
              }).catch((error) => {
                console.log(error)
              }).then(() => {
                localStorage.removeItem("data");
                localStorage.setItem("data", JSON.stringify([]));
                var event = new Event('basketDeleted');
                window.dispatchEvent(event);
                this.setState({ basket_data: [], basket_total: 0 })
              })
          }).catch((error) => {
            console.log(error)
          })
      }))
     

    return true



  }

  render() {

    // eslint-disable-next-line
    const { children, ...attributes } = this.props;

    return (

      <React.Fragment>
        {this.storageListener()}

        <TabContent activeTab={this.state.activeTab}>

          <TabPane tabId="4" className="p-1">
            <h4>Basket</h4>
            {
              this.basketByDist().map((dist, no_key) => {

                return (
                  <div className="aside-options" key={no_key}>
                    <div className="clearfix mt-4">
                      <hr></hr>
                      <div>
                        <small className="text-muted">Products from Distributor: </small>
                        <h6>{dist.uFname}</h6>
                      </div>
                      <hr></hr>
                      {
                        dist.data.map((item, key) => {
                          return (
                            <div>
                              <small>
                                <b>{item.pName}</b>
                              </small>
                              <div className={'float-right'}>
                                <small>
                                  Quantity: {item.quantity > 1 ? <Button outline color='info' className=" cui-chevron-left icons font-2xs " size="sm" onClick={() => { this.decQuant(item.pID) }} ></Button> : null}
                                  {" " + item.quantity + " "}
                                  <Button outline color='info' className=" cui-chevron-right icons font-2xs " size="sm" onClick={() => { this.incQuant(item.pID) }} ></Button>
                                  {" "}
                                  <Button outline color='danger' className=" cui-circle-x icons font-sm " size="sm" onClick={() => { this.deleteItem(item.pID) }} ></Button>
                                </small>
                              </div>
                              <div>
                                <small className="text-muted">
                                  {item.pDesc}
                                </small>
                              </div>
                              <div>
                                <small className="text-muted">
                                  {"€" + (item.pPrice * item.quantity).toFixed(2)}
                                </small>
                              </div>
                            </div>
                          )
                        })
                      }

                    </div>
                  </div>
                )
              }
              )
            }
            <br></br>
            <hr></hr>
            <h5>Total: {"€" + this.state.basket_total.toFixed(2)}</h5>
            <Button disabled={this.state.basket_data.length === 0} block color="success" onClick={this.toggleSuccess} className="">Checkout</Button>
            <Button disabled={this.state.basket_data.length === 0} block color="danger" onClick={this.clearBasket} className="">Clear Basket</Button>
            <Modal isOpen={this.state.success} toggle={this.toggleSuccess}
              className={'modal-success ' + this.props.className}>
              <ModalHeader toggle={this.toggleSuccess}>CHECKOUT</ModalHeader>
              <ModalBody>
                Are you sure you want to checkout this order?
              </ModalBody>
              <ModalFooter>
                <Button color="success" onClick={this.checkout}>Yes</Button>{' '}
                <Button color="secondary" onClick={this.toggleSuccess}>Continue shopping</Button>
              </ModalFooter>
            </Modal>
          </TabPane>
        </TabContent>
      </React.Fragment>
    );
  }
}

DefaultAside.propTypes = propTypes;
DefaultAside.defaultProps = defaultProps;

export default DefaultAside;
