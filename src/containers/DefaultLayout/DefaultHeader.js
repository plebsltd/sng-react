import React, { Component } from 'react';
import { DropdownItem, DropdownMenu, DropdownToggle, Nav, NavItem, NavLink } from 'reactstrap';
import PropTypes from 'prop-types';
import { AppAsideToggler, AppHeaderDropdown, AppSidebarToggler } from '@coreui/react';
import rp from 'request-promise';
import { Image} from 'cloudinary-react';
import Cookies from 'universal-cookie';
const cookies = new Cookies();


const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};

var token = '';

class DefaultHeader extends Component {

  constructor(props) {
    super(props);
    
    if(JSON.parse(localStorage.getItem("data")).length === undefined){
      localStorage.setItem("data", [])
    }
    this.state = {
      basket_items: JSON.parse(localStorage.getItem("data")).length,
      data: {}
    }
    this.storageListener = this.storageListener.bind(this)
    this.getData = this.getData.bind(this)
  }
  storageListener = () => {
    var that = this;
    window.addEventListener("itemInserted", function () {
      that.setState({ basket_items: JSON.parse(localStorage.getItem("data")).length })
      that.state.basket_items = JSON.parse(localStorage.getItem("data")).length

    }, false)
    window.addEventListener("itemDeleted", function () {
      that.setState({ basket_items: JSON.parse(localStorage.getItem("data")).length })
      that.state.basket_items = JSON.parse(localStorage.getItem("data")).length

    }, false)
    window.addEventListener("basketDeleted", function () {
      that.setState({ basket_items: JSON.parse(localStorage.getItem("data")).length })
      that.state.basket_items = JSON.parse(localStorage.getItem("data")).length
    }, false)
  }

  getData() {
    var data = []
    var user_details = {
      uri: "https://sng-heroku-api.herokuapp.com/api/user_details",
      headers: {
        Authorization: 'Bearer ' + token
      },
      method: 'GET',
      json: true
    }
    rp(user_details).then(

      (ud) => {

        data = ud.data[0]
        this.setState({ data: data })
        return ud

      }).catch((error) => {
        if (error.statusCode === 403)
          this.toggleEXP()
      })

    return null
  }

  componentWillMount(){
    if (localStorage.getItem("id_token")) {
      token = localStorage.getItem("id_token")
    }
    
    if (cookies.get('id_token')) {
      token = cookies.get('id_token')
    }
    this.getData()
  }

  render() {

    // eslint-disable-next-line
    const { children, ...attributes } = this.props;

    return (
      <React.Fragment>
        {this.storageListener()}
        <AppSidebarToggler className="d-lg-none" display="md" mobile />
        <AppSidebarToggler className="d-md-down-none" display="lg" />

        <Nav className="d-md-down-none" navbar>
          <NavItem className="px-0">
            <NavLink href="/"><img className="navbar-brand-full" src="../../assets/img/logo.png" height="26" alt="CoreUI Logo"/></NavLink>
          </NavItem>
        </Nav>
        <Nav className="ml-auto" navbar>
          <AppHeaderDropdown direction="down">
            <DropdownToggle nav>
              <Image cloudName="xatzisktv" publicId={this.state.data.udIcon} radius="400" width="35" />
            </DropdownToggle>
            <DropdownMenu right style={{ right: 'auto' }}>
              <DropdownItem tag="a" href="#/settings"><i className="fa fa-wrench"></i> Settings </DropdownItem>
              <DropdownItem onClick={e => { localStorage.removeItem("id_token"); cookies.remove("id_token"); this.props.onLogout(e) }}><i className="fa fa-lock"></i> Logout</DropdownItem>
            </DropdownMenu>
          </AppHeaderDropdown>
        </Nav>
        <div style={{ 'width': '10px' }}></div>
        {localStorage.getItem("userType") === '0' ? <div className="badge"><AppAsideToggler className="d-md-down" basket_items={this.state.basket_items} /> </div> : null}
      </React.Fragment>
    );
  }
}

DefaultHeader.propTypes = propTypes;
DefaultHeader.defaultProps = defaultProps;

export default DefaultHeader;
