import React from 'react';
import DefaultLayout from './containers/DefaultLayout';

const OrderHistory = React.lazy(() => import('./views/OrderHistory'));
const OrderProducts = React.lazy(() => import('./views/OrderProducts'));
const DistOrderProducts = React.lazy(() => import('./views/DistOrderProducts'));
const Products = React.lazy(() => import('./views/Products'));
const Settings = React.lazy(() => import('./views/Pages/Settings'));
const Dashboard = React.lazy(() => import('./views/Dashboard'));
const DistProducts = React.lazy(() => import('./views/DistProducts'));
const DistHistory = React.lazy(() => import('./views/DistHistory'));
const Page404 = React.lazy(() => import('./views/Pages/Page404'));
// const Landing = React.lazy(() => import('./views/Landing'))

const routes = [
  { path: '/', exact: true, name: 'Home', component: DefaultLayout },
  { path: '/dashboard', name: 'Dashboard', component: Dashboard },
  { path: '/disthistory', name: 'DistHistory', component: DistHistory },
  { path: '/orderhistory', name: 'OrderHistory', component: OrderHistory },
  { path: '/distorderproducts/:orderid', name: 'DistOrderProducts', component: DistOrderProducts },
  { path: '/orderproducts/:orderid', name: 'OrderProducts', component: OrderProducts },
  { path: '/products/:did', name: 'Products', component: Products },
  { path: '/products', name: 'Products', component: Products },
  { path: '/settings', name: 'Settings', component: Settings },
  { path: '/distproducts', name: 'DistProducts', component: DistProducts },
  { path: '/404', name: 'Page404', component: Page404 }
];

export default routes;
