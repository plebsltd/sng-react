import React, { Component } from 'react';
import { Line } from 'react-chartjs-2';
import { Button, Card, CardBody, CardHeader, CardTitle, Col, Progress, Row, Table, Modal, ModalHeader, ModalFooter, ModalBody } from 'reactstrap';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities'
import rp from 'request-promise';
import { ToastContainer, toast } from 'react-toastify';
// eslint-disable-next-line
import toastifyFont from 'react-toastify/dist/ReactToastify.css';
import Cookies from 'universal-cookie';
const cookies = new Cookies();

const brandPrimary = getStyle('--primary')

const brandInfo = getStyle('--info')


localStorage.__proto__.addItem = function (item) {
  localStorage.setItem('data', item)
  var event = new Event('itemInserted');
  window.dispatchEvent(event);

}

var token = '';


class Dashboard extends Component {
  constructor(props) {
    super(props);

    this.state = {
      dropdownOpen: false,
      radioSelected: 2,
      max: 1000,
      topdist: [],
      topprod: [],
      redirect: false,
      week_amount_total: 0,
      currentBigData: {},
      big_data_opts: {},
      month_data: {},
      year_data: {},
      modalEXP: false
    };

    this.basket = [];

    this.toggle = this.toggle.bind(this);
    this.getBigData = this.getBigData.bind(this)
    this.getData = this.getData.bind(this)
    this.renderRedirect = this.renderRedirect.bind(this)
    this.setRedirect = this.setRedirect.bind(this)
    this.toggleEXP = this.toggleEXP.bind(this);
    this.handleChangesEXP = this.handleChangesEXP.bind(this)

  }

  toggleEXP() {
    this.setState({
      modalEXP: !this.state.modalEXP
    });
  }

  handleChangesEXP() {
    // this.toggleEXP()
    this.props.history.push('../Login')
    window.location.reload();

  }


  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen,
    });
  }

  getData = () => {
    var dashboard = {
      uri: "https://sng-heroku-api.herokuapp.com/api/dashboard",
      headers: {
        Authorization: 'Bearer ' + token
      },
      method: 'GET',
      json: true
    }

    rp(dashboard).then(
      (res) => {

        var chart_money_data = []
        var chart_quant_data = []
        var labels = []
        var monthlabels = [];
        var monthamounts = [];
        var ydata = []
        var ylabels = [];
        var ytotals = [];

        //  Top 5 Products
        var topProd = res.data.topProd
        this.setState({
          topprod: topProd
        })

        //  Top 5 Distributors
        var topdist = res.data.topDist
        this.setState({
          topdist: topdist
        })

        //  Weekly Data
        var week = res.data.weekData

        var weekProd = res.data.weekProdData

        weekProd.forEach((e) => {
          chart_quant_data.push(parseFloat(parseFloat(e.products)))  
        })

        //  Populate weekly chart arrays
        week.forEach((e) => {
          labels.push(e.date)
          chart_money_data.push(parseFloat(parseFloat(e.money).toFixed(2)))
          
        })

        //  Add 0 if array length is 1
        if (chart_money_data.length === 1) {
          chart_money_data.unshift(parseFloat(0))
        }

        if (chart_quant_data.length === 1) {
          chart_quant_data.unshift(parseFloat(0))
        }

        this.setState({
          week_amount_total: chart_money_data.reduce((a, b) => a + b, 0),
          week_products_total: chart_quant_data.reduce((a, b) => a + b, 0)
        })

        //  Chart week options and labels
        this.setState(
          {
            chart_week_money: {
              labels: labels,
              datasets:
                [
                  {
                    label: '€',
                    backgroundColor: brandPrimary,
                    borderColor: 'rgba(255,255,255,.55)',
                    data: chart_money_data
                  }
                ],
            }
          })

        this.setState(
          {
            chart_week_products: {
              labels: labels,
              datasets:
                [
                  {
                    label: 'Products:',
                    backgroundColor: brandInfo,
                    borderColor: 'rgba(255,255,255,.55)',
                    data: chart_quant_data
                  }
                ],
            },

          })


        this.setState(
          {
            week_prod_opts: {

              tooltips: {
                enabled: false,
                custom: CustomTooltips
              },
              maintainAspectRatio: false,
              legend: {
                display: false,
              },
              scales: {
                xAxes: [
                  {
                    gridLines: {
                      color: 'transparent',
                      zeroLineColor: 'transparent',
                    },
                    ticks: {
                      fontSize: 2,
                      fontColor: 'transparent',
                    },

                  }],
                yAxes: [
                  {
                    display: false,
                    ticks: {
                      display: false,
                      min: Math.min.apply(Math, this.state.chart_week_products.datasets[0].data) - 5,
                      max: Math.max.apply(Math, this.state.chart_week_products.datasets[0].data) + 5,
                    },
                  }],
              },
              elements: {
                line: {
                  tension: 0.00001,
                  borderWidth: 1,
                },
                point: {
                  radius: 4,
                  hitRadius: 10,
                  hoverRadius: 4,
                },
              },
            }
          })

        this.setState(
          {
            week_money_opts: {
              tooltips: {
                enabled: false,
                custom: CustomTooltips
              },
              maintainAspectRatio: false,
              legend: {
                display: false,
              },
              scales: {
                xAxes: [
                  {
                    gridLines: {
                      color: 'transparent',
                      zeroLineColor: 'transparent',
                    },
                    ticks: {
                      fontSize: 2,
                      fontColor: 'transparent',
                    },

                  }],
                yAxes: [
                  {
                    display: false,
                    ticks: {
                      display: false,
                      min: Math.min.apply(Math, this.state.chart_week_money.datasets[0].data) - 5,
                      max: Math.max.apply(Math, this.state.chart_week_money.datasets[0].data) + 5,
                    },
                  }],
              },
              elements: {
                line: {
                  tension: 0.00001,
                  borderWidth: 1,
                },
                point: {
                  radius: 4,
                  hitRadius: 10,
                  hoverRadius: 4,
                },
              },
            }
          })

        var monthdata = res.data.monthData
        monthdata.forEach((e) => {
          monthlabels.push(e.month + " " + e.year)
          monthamounts.push(parseFloat(parseFloat(e.total).toFixed(2)))
        })
        if (monthamounts.length === 1) {
          monthamounts.unshift(parseFloat(0))
        }

        this.setState(
          {
            month_data: {
              labels: monthlabels,
              datasets: [{
                label: '€',
                backgroundColor: hexToRgba(brandInfo, 10),
                borderColor: brandInfo,
                pointHoverBackgroundColor: '#fff',
                borderWidth: 2,
                data: monthamounts,
              }]
            }
          })

          this.setState({
            currentBigDataMonth: {
              labels: monthlabels,
              datasets: [{
                label: '€',
                backgroundColor: hexToRgba(brandInfo, 10),
                borderColor: brandInfo,
                pointHoverBackgroundColor: '#fff',
                borderWidth: 2,
                data: monthamounts,
              }]
            }
          })

        this.setState({
          bigDataCurrentView_month: monthlabels[0] + " - " + monthlabels[monthlabels.length - 1],
          bigDataCurrentView: monthlabels[0] + " - " + monthlabels[monthlabels.length - 1]
        })

        var that = this

        ydata = res.data.yearData
        ydata.forEach((e) => {
          ylabels.push(e.year)
          ytotals.push(parseFloat(parseFloat(e.total).toFixed(2)))
        })

        if (ytotals.length === 1) {
          ytotals.unshift(0);
        }

        that.setState({
          bigDataCurrentView_year: ylabels[0] + " - " + ylabels[ylabels.length - 1],
          all_time_total: ytotals.reduce((a, b) => a + b, 0),
          year_data: {
            labels: ylabels,
            datasets: [{
              label: '€',
              backgroundColor: hexToRgba(brandInfo, 10),
              borderColor: brandInfo,
              pointHoverBackgroundColor: '#fff',
              borderWidth: 2,
              data: ytotals,
            }]
          },
          max: Math.max(...ytotals),
        })
        that.getBigData();

        that.setState({
          currentBigDataYear: {
            labels: ylabels,
            datasets: [{
              label: '€',
              backgroundColor: hexToRgba(brandInfo, 10),
              borderColor: brandInfo,
              pointHoverBackgroundColor: '#fff',
              borderWidth: 2,
              data: ytotals,
            }]
          }
        })

      }).catch((error) => {
        if (error.statusCode === 403)
          this.toggleEXP();
      })
  }

  getBigData = () => {

    this.setState({
      big_data_opts:
      {
        tooltips: {
          enabled: false,
          custom: CustomTooltips,
          intersect: true,
          mode: 'index',
          position: 'nearest',
          callbacks: {
            labelColor: function (tooltipItem, chart) {
              return { backgroundColor: chart.data.datasets[tooltipItem.datasetIndex].borderColor }
            }
          }
        },
        maintainAspectRatio: false,
        legend: {
          display: false,
        },
        scales: {
          xAxes: [
            {
              gridLines: {
                drawOnChartArea: false,
              },
            }],
          yAxes: [
            {
              ticks: {
                beginAtZero: true,
                maxTicksLimit: 5,
                stepSize: this.state.max / 10,
                max: this.state.max,
              },
            }],
        },
        elements: {
          point: {
            radius: 0,
            hitRadius: 10,
            hoverRadius: 4,
            hoverBorderWidth: 3,
          },
        },
      }
    });
  }

  downloadCSV = (dataa) => {
    let csvContent = "data:text/csv;charset=utf-8,";
    var data = [...dataa.datasets[0].data]
    var labels = [...dataa.labels]

    for (var i = 0; i < data.length; i++) {
      csvContent += labels[i] + "," + data[i] + "\r\n";
    }

    var encodedUri = encodeURI(csvContent);
    var link = document.createElement("a");
    link.setAttribute("href", encodedUri);
    link.setAttribute("download", "Current_Data.csv");
    document.body.appendChild(link);
    link.click();
  }

  setRedirect = (item) => {
    this.setState({
      redirect: true,
      distributorid: item.udID
    })
  }

  renderRedirect = () => {
    if (this.state.redirect) {
      this.props.history.push(`../../Products/${this.state.distributorid}`, { udID: this.state.distributorid })
    }
  }

  componentDidMount() {

  }

  componentWillMount() {
    

    if (localStorage.getItem("id_token")) {
      token = localStorage.getItem("id_token")
    }

    if (cookies.get('id_token')) {
      token = cookies.get('id_token')
    }

    if (cookies.get('id_token') === undefined && localStorage.getItem('id_token') === null) {
      this.props.history.push('../Login')

    }
    this.getData()
  }

  addProduct(item) {
    this.basket = JSON.parse(localStorage.getItem("data"))

    var prod_found = false
    for (var elem of this.basket) {
      if (item.pID === elem.pID) {
        prod_found = true
        elem.quantity += 1
        break
      }
    }

    if (!prod_found) {
      item.quantity = 1
      this.basket.push(item)
    }

    localStorage.addItem(JSON.stringify(this.basket))
    toast.info(item.pName + " has been added to your basket!", {
      position: toast.POSITION.BOTTOM_CENTER,
      autoClose: 3000
    });
  }

  loading = () => <div className="animated fadeIn pt-1 text-center">Loading...</div>

  render() {
    if (localStorage.getItem("userType") === '1') {
      this.props.history.replace('/404')
      return null
    }
    else {


      return (
        <div className="animated fadeIn">
          <ToastContainer />
          {this.renderRedirect()}
          <Row>
            <Col xs="12" sm="6" lg="3">
              <Card className="text-white bg-info">
                <CardBody className="pb-0">
                  <div className="text-value">{this.state.week_products_total}</div>
                  <div>Products bought last week</div>
                </CardBody>
                <div className="chart-wrapper mx-3" style={{ height: '70px' }}>
                  <Line data={this.state.chart_week_products} options={this.state.week_prod_opts} height={70} />
                </div>
              </Card>
            </Col>

            <Col xs="12" sm="6" lg="3">
              <Card className="text-white bg-primary">
                <CardBody className="pb-0">
                  <div className="text-value">€{(this.state.week_amount_total).toFixed(2)}</div>
                  <div>Spent last week</div>
                </CardBody>
                <div className="chart-wrapper mx-3" style={{ height: '70px' }}>
                  <Line data={this.state.chart_week_money} options={this.state.week_money_opts} height={70} />
                </div>
              </Card>
            </Col>
          </Row>
          <Row>
            <Col>
              <Card>
                <CardBody>
                  <Row>
                    <Col sm="5">
                      <CardTitle className="mb-0">Total purchases (Monthly)</CardTitle>
                      <div className="small text-muted">{this.state.bigDataCurrentView}</div>
                    </Col>
                    <Col sm="7" className="d-none d-sm-inline-block">
                      <Button onClick={() => this.downloadCSV(this.state.currentBigDataMonth)} color="primary" className="float-right"><i className="icon-cloud-download"></i></Button>
                      {/* <ButtonToolbar className="float-right" aria-label="Toolbar with button groups">
                        <ButtonGroup className="mr-3" aria-label="First group">
                          <Button color="outline-secondary" onClick={() => this.onRadioBtnClick(1)} active={this.state.radioSelected === 1}>Monthly</Button>
                          <Button color="outline-secondary" onClick={() => this.onRadioBtnClick(2)} active={this.state.radioSelected === 2}>Annually</Button>
                        </ButtonGroup>
                      </ButtonToolbar> */}
                    </Col>
                  </Row>
                  <div className="chart-wrapper" style={{ height: 300 + 'px', marginTop: 40 + 'px' }}>
                    <Line data={this.state.month_data} options={this.state.big_data_opts} height={300} />
                  </div>
                </CardBody>
              </Card>
            </Col>
          </Row>

          <Row>
            <Col>
              <Card>
                <CardBody>
                  <Row>
                    <Col sm="5">
                      <CardTitle className="mb-0">Total purchases (Yearly)</CardTitle>
                      <div className="small text-muted">{this.state.bigDataCurrentView}</div>
                    </Col>
                    <Col sm="7" className="d-none d-sm-inline-block">
                      <Button onClick={() => this.downloadCSV(this.state.currentBigDataYear)} color="primary" className="float-right"><i className="icon-cloud-download"></i></Button>
                      {/* <ButtonToolbar className="float-right" aria-label="Toolbar with button groups">
                        <ButtonGroup className="mr-3" aria-label="First group">
                          <Button color="outline-secondary" onClick={() => this.onRadioBtnClick(1)} active={this.state.radioSelected === 1}>Monthly</Button>
                          <Button color="outline-secondary" onClick={() => this.onRadioBtnClick(2)} active={this.state.radioSelected === 2}>Annually</Button>
                        </ButtonGroup>
                      </ButtonToolbar> */}
                    </Col>
                  </Row>
                  <div className="chart-wrapper" style={{ height: 300 + 'px', marginTop: 40 + 'px' }}>
                    <Line data={this.state.year_data} options={this.state.big_data_opts} height={300} />
                  </div>
                </CardBody>
              </Card>
            </Col>
          </Row>

          <Row>
            <Col>
              <Card>
                <CardHeader>
                  Recommended for you
              </CardHeader>
                <CardBody>
                  <Table hover responsive className="table-outline mb-0 d-none d-sm-table table-hover">
                    <thead className="thead-light">
                      <tr>
                        <th className="text-center"><i className="icon-star"></i></th>
                        <th>Distributor</th>
                        <th className="text-center">Money Spent (€)</th>
                        <th>Percentage (%) of your total amount spent on the platform</th>
                      </tr>
                    </thead>
                    <tbody>
                      {
                        this.state.topdist.map((item, key) => {
                          return (
                            <tr key={key} onClick={() => { this.setRedirect(item) }} style={{ cursor: 'pointer' }}>
                              <td className="text-center">
                                <div className="badge">
                                  <i className="fa fa-truck fa-3x"></i>
                                  <span className="badge badge-info">{key + 1}</span>
                                </div>
                              </td>
                              <td>
                                <div>{item.distributor}</div>
                                <div className="small text-muted">
                                  <span>Email</span> : {item.email}
                                </div>
                              </td>
                              <td className="text-center">
                                {"€" + item.total.toFixed(2)}
                              </td>
                              <td>
                                <div className="clearfix">
                                  <div className="float-left">
                                    <strong>{((item.total / this.state.all_time_total) * 100).toFixed(2) + "%"}</strong>
                                  </div>
                                  <div className="float-right">
                                    <small className="text-muted">{item.first_order + " - " + item.last_order}</small>
                                  </div>
                                </div>
                                <Progress className="progress-xs" color="info" value={((item.total / this.state.all_time_total) * 100)} />
                              </td>
                            </tr>
                          )
                        })
                      }
                    </tbody>
                  </Table>
                  <br></br>
                  <Table hover responsive className="table-outline mb-0 d-none d-sm-table table-hover">
                    <thead className="thead-light">
                      <tr>
                        <th className="text-center"><i className="icon-star"></i></th>
                        <th>Product</th>
                        <th className="text-center">Price per unit (€)</th>
                        <th className="text-center">VAT (%)</th>
                        <th className="text-center">Times bought</th>
                        <th className="text-center"></th>
                      </tr>
                    </thead>
                    <tbody>
                      {
                        this.state.topprod.map((item, key) => {
                          return (
                            <tr key={key}>
                              <td className="text-center">
                                <div className="badge">
                                  <i className="fa fa-shopping-bag fa-3x"></i>
                                  <span className="badge badge-warning">{key + 1}</span>
                                </div>
                              </td>
                              <td>
                                <div>{item.product}</div>
                                <div className="small text-muted">
                                  <span>Description</span> : {item.description}
                                </div>
                              </td>
                              <td className="text-center">
                                {"€" + item.price.toFixed(2)}
                              </td>
                              <td className="text-center">
                                {item.vat * 100 + "%"}
                              </td>
                              <td className="text-center">
                                {item.total}
                              </td>
                              <td className="text-right">
                                {item.pStock === 1 ? (
                                  <Button onClick={() => { this.addProduct(item) }} className={"btn btn-ghost-primary"}>Add to basket <i className="icon-basket-loaded icons font-xl"></i></Button>
                                ) : (
                                    <Button className={"btn disabled btn-ghost-disabled"}>Out of Stock <i className="icon-ban icons font-xl"></i></Button>
                                  )
                                }
                              </td>
                            </tr>
                          )
                        })
                      }
                    </tbody>
                  </Table>
                </CardBody>
              </Card>
            </Col>
          </Row>

          <Modal className="modal-info" isOpen={this.state.modalEXP} toggle={this.toggleEXP}>
            <ModalHeader toggle={this.toggleEXP}>Session Expired</ModalHeader>
            <ModalBody>
              Your session has expired. Please log in to continue.
          </ModalBody>
            <ModalFooter>
              <Button color="info" onClick={this.handleChangesEXP}>Yes</Button>
            </ModalFooter>
          </Modal>
        </div>
      )
    }
  }
}

export default Dashboard;