import React, { Component } from 'react';
import { Card, CardBody, CardHeader, Col, Row, Table, Input, Modal, ModalHeader, ModalFooter, ModalBody, Button } from 'reactstrap';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import rp from 'request-promise';
import Cookies from 'universal-cookie';
const cookies = new Cookies();


var token = '';


class DistHistory extends Component {
  constructor(props) {
    super(props)
    this.state = {
      redirect: false,
      userID: null,
      token: null,
      sort: 'desc',
      status: "ALL",
      data: [],
      modalEXP: false
    }

    this.compareBy = this.compareBy.bind(this);
    //this.sortBy = this.sortBy.bind(this);
    this.toggleEXP = this.toggleEXP.bind(this);
    this.handleChangesEXP = this.handleChangesEXP.bind(this)

  }

  toggleEXP() {
    this.setState({
      modalEXP: !this.state.modal
    });
  }

  handleChangesEXP() {
    this.toggleEXP()
    this.props.history.push('../Login')
    window.location.reload();

  }


  setRedirect = (item) => {
    this.setState({
      redirect: true,
      order: item,
      orderid: item.ohID
    })
  }

  renderRedirect = () => {
    if (this.state.redirect) {
      this.props.history.push(`./DistOrderProducts/${this.state.orderid}`, { orderid: this.state.order.ohID, order: this.state.order, userID: this.state.userID, token: this.state.token })
    }
  }

  compareBy(key) {
    return function (a, b) {
      if (a[key] < b[key]) return -1;
      if (a[key] > b[key]) return 1;
      return 0;
    };
  }

  // sortBy(key) {
  //   const arrayCopy = [...this.state.data];
  //   if (this.state.sort === 'desc') {
  //     this.setState({ sort: 'asc' })
  //     arrayCopy.sort(this.compareBy(key));

  //   }

  //   if (this.state.sort === 'asc') {
  //     this.setState({ sort: 'desc' })
  //     arrayCopy.reverse(this.compareBy(key));
  //   }

  //   this.setState({ data: arrayCopy });
  // }



  getData = () => {
    var data = []
    var orders = {
      uri: "https://sng-heroku-api.herokuapp.com/api/dist_orders",
      headers: {
        Authorization: 'Bearer ' + token
      },
      method: 'GET',
      json: true
    }
    rp(orders).then(

      (ord) => {

        data = ord.data
        this.props.location.state = { data: data };
        this.setState({ data: data })

        return ord;
      }
    ).catch((error) => {
      this.toggleEXP()
    })
    return true;
  }

  stampToDate(stamp) {
    var date = new Date(stamp);

    return date;
  }

  onSearch(e) {
    var data = this.props.location.state.data.filter((oh) => {

      if (e.target.value === "") {
        this.setState({ status: "ALL" })
        return true
      }
      else {
        if(!isNaN(e.target.value)){
          return oh.ohID === parseInt(e.target.value)
        }
        else{
          return oh.uFname.toLowerCase().includes(e.target.value.toLowerCase())    
        }
        
      }


    })
    this.setState({ data: data })
  }

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
    var data = this.props.location.state.data.filter((oh) => {

      if (e.target.value === "ALL") {

        return true
      }
      else {
        return oh.ohStatus.includes(e.target.value)
      }


    })
    this.setState({ data: data })
  };



  componentWillMount() {
    if (localStorage.getItem("id_token")) {
      token = localStorage.getItem("id_token")
    }
    
    if (cookies.get('id_token')) {
      token = cookies.get('id_token')
    }
    
    if (cookies.get('id_token') === undefined && localStorage.getItem('id_token') === null) {
      this.props.history.push('../Login')

    }

    this.getData()

  }


  render() {
    if (localStorage.getItem("userType") === '0' && (cookies.get('id_token') !== undefined || localStorage.getItem('id_token') !== null)) {
      this.props.history.replace('/404')
      return null
    }

    else {

      return (
        <div className="animated fadeIn">

          {this.renderRedirect()}
          <Row>
            <Col xs="12" lg="12">
              <Card>
                <CardHeader >
                  <i className="fa fa-align-justify "></i> Order History (Order View)
                <Input className="col-sm-2 float-right" placeholder="Search" onChange={(e) => { this.onSearch(e) }} />

                  <Select style={{ fontSize: '90%' }} onChange={(e) => { this.handleChange(e) }} value={this.state.status} className="col-sm-2 float-right" inputProps={{ name: 'status', id: 'status' }}>
                    <MenuItem style={{ fontSize: '90%' }} value="ALL">ALL</MenuItem>
                    <MenuItem style={{ fontSize: '90%' }} value="CONFIRMED">CONFIRMED <i className={`fa fa-circle fa-lg text-success`} /></MenuItem>
                    <MenuItem style={{ fontSize: '90%' }} value="PENDING">PENDING <i className={`fa fa-circle fa-lg text-warning`} /></MenuItem>
                    <MenuItem style={{ fontSize: '90%' }} value="CANCELED">CANCELED <i className={`fa fa-circle fa-lg text-danger`} /></MenuItem>
                  </Select>

                </CardHeader>
                <CardBody>
                  <Table className="table table-hover">
                    <thead>
                      <tr>
                        <th scope="col"  style={{ cursor: 'pointer' }}>OrderID</th>
                        <th scope="col"  style={{ cursor: 'pointer' }}>Date</th>
                        <th scope="col"  style={{ cursor: 'pointer' }}>User</th>
                        <th scope="col"  style={{ cursor: 'pointer' }}>Amount</th>
                        <th scope="col"  style={{ cursor: 'pointer' }}>Status</th>
                      </tr>
                    </thead>


                    <tbody>

                      {this.state.data.map(function (item, key) {

                        if (item.ohStatus === "PENDING") {
                          this.status = "text-warning"
                        }
                        if (item.ohStatus === 'CONFIRMED') {
                          this.status = "text-success";
                        }

                        if (item.ohStatus === 'EDITED') {
                          this.status = "text-primary";
                        }

                        if (item.ohStatus === 'CANCELED') {
                          this.status = "text-danger";
                        }

                        return (

                          <tr onClick={() => { this.setRedirect(item) }} key={key} className={`text-dark`} style={{ cursor: 'pointer' }}>
                            <td>{item.ohID}</td>
                            <td>{this.stampToDate(item.ohTimestamp.toString()).toLocaleString()} </td>
                            <td>{item.uFname}</td>
                            <td>{new Intl.NumberFormat('en-GB', {
                              style: 'currency',
                              currency: 'EUR',
                              minimumFractionDigits: 2,
                              maximumFractionDigits: 2
                            }).format(item.ohTotal)}</td>
                            <td>{item.ohStatus} <i className={`fa fa-circle fa-lg ${this.status}`} /></td>
                          </tr>
                        )
                      }.bind(this))}

                    </tbody>
                  </Table>
                </CardBody>
              </Card>
            </Col>
          </Row>
          <Modal className="modal-info" isOpen={this.state.modalEXP} toggle={this.toggleEXP}>
            <ModalHeader toggle={this.toggleEXP}>Session Expired</ModalHeader>
            <ModalBody>
              Your session has expired. Please log in to continue. 
          </ModalBody>
            <ModalFooter>
              <Button color="info" onClick={this.handleChangesEXP}>Yes</Button>
            </ModalFooter>
          </Modal>
        </div>






      );
    }
  }
}

export default DistHistory;
