import React, { Component } from 'react';
import { Card, CardBody, CardHeader, CardTitle, Col, Row, Table, Button, Modal, ModalHeader, ModalBody, ModalFooter, Input, InputGroupAddon, InputGroupText, InputGroup } from 'reactstrap';
import rp from 'request-promise';
import Cookies from 'universal-cookie';
import { ToastContainer, toast } from 'react-toastify';
import pdfMake from 'pdfmake/build/pdfmake.js';
import pdfFonts from 'pdfmake/build/vfs_fonts';
// eslint-disable-next-line
import toastifyFont from 'react-toastify/dist/ReactToastify.css';

import { Image} from 'cloudinary-react';

const imageToURI = require('image-to-data-uri')
const cookies = new Cookies();

pdfMake.vfs = pdfFonts.pdfMake.vfs;

var data = []
var orderID = 0

var token = '';

class DistOrderProducts extends Component {

  constructor(props) {
    super(props)
    this.props = props
    this.state = {
      data: [],
      sort: '',
      orderTotal: 0,
      OH_Confirmation: "",
      modal: false,
      modalc: false,
      user_data: [],
      modalEXP: false
    }

    this.onDiscountChange = this.onDiscountChange.bind(this);
    this.toggle = this.toggle.bind(this);
    this.togglec = this.togglec.bind(this);
    this.compareBy.bind(this);
    //this.sortBy.bind(this);
    this.saveData = this.saveData.bind(this);
    this.confimOrder = this.confimOrder.bind(this);
    this.getData = this.getData.bind(this)
    this.toggleEXP = this.toggleEXP.bind(this);
    this.handleChangesEXP = this.handleChangesEXP.bind(this)
    this.makePDF = this.makePDF.bind(this);

  }

  toggleEXP() {
    this.setState({
      modalEXP: !this.state.modal
    });
  }

  handleChangesEXP() {
    this.toggleEXP()
    this.props.history.push('../Login')
    window.location.reload();

  }

    makePDF() {
    var imageUri = ""
    imageToURI(`https://res.cloudinary.com/xatzisktv/image/upload/${this.state.order.udIcon}`, function (err, uri) {
      if (!err) {
        console.log(uri)
        imageUri = uri
        var pdf_data = [[
          { text: 'Product Name', style: 'tableHeader' },
          { text: 'Price', style: 'tableHeader' },
          { text: 'Quantity', style: 'tableHeader' },
          { text: 'Total before Discount', style: 'tableHeader' },
          { text: 'Discount', style: 'tableHeader' },
          { text: 'VAT', style: 'tableHeader' },
          { text: 'Total after Discount', style: 'tableHeader' }]]
        this.state.data.forEach((e) => {
          var subtotal = e.pPrice * e.opQuant;
          var subtotaladisc = e.pPrice * e.opQuant * (1 - e.opDisc);

          pdf_data.push([e.pName, "€" + e.pPrice.toFixed(2), "" + e.opQuant, "€" + subtotal.toFixed(2), + e.opDisc * 100 + "%", "" + e.pVAT * 100 + "%", "€" + subtotaladisc.toFixed(2)])
        })
        var totalaftervat = this.state.order.ohTotal.toFixed(2) * 1.19
        var pdf_data_total = []

        pdf_data_total.push([{ text: 'Total after Discount', bold: true }, "€" + this.state.order.ohTotal.toFixed(2)])
        pdf_data_total.push([{ text: 'Total after VAT', bold: true }, { text: "€" + totalaftervat.toFixed(2), bold: true }])

        var docDefinition = {
          footer: {
            margin: [30,0,0,0],
            columns: [
              {
                text:[
                  {text:"Supply And Go © 2019 Supply And Go.\n",style:"small"},
                  {text:"For any enquiries please contact us at: info@supplyandgo.com",style:"small"}
                ]
              }
            ]
          },
          content: [
            {
              alignment: 'justify',
              columns: [
                {
                  text: [{
                    text: 'Invoice\n',
                    style: 'header'
                  }, {
                    text: 'Powered by Supply and Go\n\n\n',
                    style: 'small'
                  }]

                },
                {

                  width: 100,
                  image: imageUri
                }
                ,
              ]
            },
            {
              alignment: 'justify',
              columns: [
                {
                  text: [{ text: `From`, bold: true }, `\n${this.state.order.uFname}\n${this.state.order.udAddress}\n${this.state.order.udPhone}`],
                  style: 'header2'
                },
                {
                  text: [{ text: `To`, bold: true }, `\n${this.state.user_data.uFname}\n${this.state.user_data.udAddress}\n${this.state.user_data.udPhone}`],
                  style: 'header2'
                }
              ]
            },
            '\n',
            {
              alignment: 'justify',
              columns: [
                {
                  text: `Order Number: ${this.state.order.ohID}\nDate: ${new Date(this.state.order.ohTimestamp).toUTCString().slice(0, 16)}`
                }
              ]
            },
            '\n',
            {
              table: {
                widths: [170, '*', 48, 50, 48, 30, 60],
                headerRows: 1,
                body:
                  pdf_data

              }
            },
            {
              table: {
                widths: ['*', 60],
                body:
                  pdf_data_total

              }
            },
            '\n\n\n',
            {
              alignment: 'left',
              columns: [

                {
                  width: 390,
                  text: `_______________________\n${this.state.order.uFname}`
                },
                { text: `_______________________\n${this.state.user_data.uFname}` }
              ]
            }

          ],
          styles: {
            header: {
              fontSize: 18,
              bold: true
            },
            header2: {
              fontSize: 13,
              italics: true,
              margin: [0, 0, 40, 0]
            },
            tableHeader: {
              fontSize: 12,
              bold: true,
              fillColor: '#20a8d8',
              color: '#ffffff'
            },
            small: {
              fontSize: 9
            }

          }

        }
        pdfMake.createPdf(docDefinition).download('Invoice_Order_No_' + this.state.order.ohID);
      }
    }.bind(this))

  }

  onDiscountChange(e, key) {
    let arrayCopy = [...this.state.data]

    var discount = 0

    if (e.target.value === "100" || e.target.value > parseInt("100")) {
      discount = 1
    }
    else if (e.target.value < parseInt("0")) {
      discount = 0
    }
    else {
      discount = e.target.value / 100
    }



    arrayCopy[key].opDisc = discount
    var total = 0
    arrayCopy.forEach((e) => {
      total += e.opQuant * e.pPrice * (1 - e.opDisc)
    })

    var order = this.state.order

    order.ohTotal = total;

    this.setState({ data: arrayCopy, order: order })
  }


  compareBy(key) {
    return function (a, b) {
      if (a[key] < b[key]) return -1;
      if (a[key] > b[key]) return 1;
      return 0;
    };
  }

  // sortBy(key) {
  //   let arrayCopy = [...this.state.data];

  //   if (this.state.sort === '' || this.state.sort === 'desc') {
  //     this.setState({ sort: 'asc' })
  //     arrayCopy.sort(this.compareBy(key));
  //   }

  //   if (this.state.sort === 'asc') {
  //     this.setState({ sort: 'desc' })
  //     arrayCopy.reverse(this.compareBy(key));
  //   }

  //   this.setState({ data: arrayCopy });
  // }

  getData = () => {

    var data = []
    var uID = ""
    var order_products = {
      uri: "https://sng-heroku-api.herokuapp.com/api/dist_order_products_by_order",
      headers: {
        data: JSON.stringify([{ "op.opOHID": this.props.match.params.orderid }]),
        Authorization: 'Bearer ' + token
      },
      json: true
    }
    rp(order_products).then(

      (ord_prod) => {

        data = ord_prod.data
        var total = 0;
        ord_prod.data.forEach((element) => {

          total += (element.opQuant * element.pPrice * element.opDisc)
        })
        this.props.location.state = { data: data, orderID: orderID, token: token, orderTotal: total }
        this.setState({ data: data, orderID: orderID, token: token, orderTotal: total })

      }
    ).catch((error) => {
      if (error.statusCode === 403)
        this.toggleEXP()
    }).then(

      () => {

        var order = {
          uri: "http://sng-heroku-api.herokuapp.com/api/order_dist",
          headers: {
            data: JSON.stringify([{ "ohID": this.props.match.params.orderid }]),
            Authorization: 'Bearer ' + token
          },
          json: true
        }
        rp(order).then(

          (ord) => {

            data = ord.data[0]
            uID = data.ohUID
            this.setState({ order: data })

            return ord
          }).catch((error) => {
            if (error.statusCode === 403)
              this.toggleEXP()
          }).then(

            () => {

              var userDetails = {
                uri: "https://sng-heroku-api.herokuapp.com/api/dist_user_details",
                headers: {
                  data: JSON.stringify([{ udID: uID }]),
                  Authorization: 'Bearer ' + token
                },
                json: true
              }
              rp(userDetails).then(

                (ud) => {

                  var user_d = ud.data.filter((e) => {
                    return e.udID === uID
                  })
                  this.setState({ user_data: user_d[0] })

                }).catch((error) => {
                  if (error.statusCode === 403)
                    this.toggleEXP()
                })
                return null
            }).catch((error) => {
              if (error.statusCode === 403)
                this.toggleEXP()
            })
            return null
      }).catch((error) => {
        if (error.statusCode === 403)
          this.toggleEXP()
      })
  }

  componentWillMount() {
    if (localStorage.getItem("id_token")) {
      token = localStorage.getItem("id_token")
    }

    if (cookies.get('id_token')) {
      token = cookies.get('id_token')
    }

    if (cookies.get('id_token') === undefined && localStorage.getItem('id_token') === null) {
      this.props.history.push('../Login')

    }
    this.setState({ data: data, orderID: orderID, token: token, order: { ohStatus: "" } })
    this.getData();
    // this.getUserData(this.state.data[0].ohUID);
  }

  toggle() {
    this.setState({
      modal: !this.state.modal
    });
  }

  togglec() {
    this.setState({
      modalc: !this.state.modalc
    });
  }

  saveData() {

    var data = [{ ohStatus: "CANCELED" }, { ohID: this.state.order.ohID }]
    var order = {
      uri: "https://sng-heroku-api.herokuapp.com/api/orders",
      headers: {
        data: JSON.stringify(data),
        Authorization: 'Bearer ' + token
      },
      json: true,
      method: 'PUT'
    }

    rp(order).then(
      (res, err) => {
        if (err) throw err;
        var order = this.state.order
        order.ohStatus = "CANCELED"

        this.setState({ order: order })

        toast.error("The order has been canceled!", {
          position: toast.POSITION.BOTTOM_CENTER,
          autoClose: 3000
        });

        this.setState({
          modal: !this.state.modal
        });

      }
    ).catch((error) => {
      if (error.statusCode === 403)
        this.toggleEXP()
    })

    return true

  }

  confimOrder() {

    var data = []
    this.state.data.forEach((e) => {
      data.push([e.opDisc, e.opOHID, e.opPID])
    })
    var order_products = {
      uri: "https://sng-heroku-api.herokuapp.com/api/order_products",
      headers: {
        data: JSON.stringify(data),
        Authorization: 'Bearer ' + token
      },
      json: true,
      method: 'PUT'
    }

    rp(order_products).then(
      (res, err) => {
        if (err) throw err;

        var orderData = [{ "ohTotal": this.state.order.ohTotal, "ohStatus": "CONFIRMED" }, { "ohID": this.state.order.ohID }]
        var order = {
          uri: "https://sng-heroku-api.herokuapp.com/api/orders",
          headers: {
            data: JSON.stringify(orderData),
            Authorization: 'Bearer ' + token
          },
          json: true,
          method: 'PUT'
        }

        rp(order).then(
          (res, err) => {
            if (err) throw err;

            this.setState({
              modalc: !this.state.modalc
            });

            this.getData();
            toast.success("The order has been confirmed!", {
              position: toast.POSITION.BOTTOM_CENTER,
              autoClose: 3000
            });

          }
        ).catch((error) => {
          if (error.statusCode === 403)
            this.toggleEXP()
        })


      }
    ).catch((error) => {
      if (error.statusCode === 403)
        this.toggleEXP()
    })

    return true

  }

  render() {
    var orderID = ""


    if (this.props === undefined) {
      orderID = "Undefined"
    }
    else {
      orderID = this.props.match.params.orderid
    }

    if (localStorage.getItem("userType") === '0' && (cookies.get('id_token') !== undefined || localStorage.getItem('id_token') !== null)) {
      this.props.history.replace('/404')
      return null
    }
    else {

      return (

        <div className="animated fadeIn">
          <ToastContainer />

          <Row>
            <Col>
              <Card>
                <CardBody>
                  <Row>
                    <Col md="auto">
                      <div className="text-center"><Image cloudName="xatzisktv" publicId={this.state.user_data.udIcon} width="80" radius="150" /></div>
                    </Col>
                    <Col md="auto">
                      <CardTitle className="mb-0">Order from: {this.state.user_data.uFname}</CardTitle>
                      <div className="small text-muted"><i className="fa fa-envelope"></i>{' Email: '}<a href={"mailto:" + this.state.user_data.udEmail}>{this.state.user_data.udEmail}</a></div>
                      <div className="small text-muted"><i className="fa fa-phone"></i>{' Phone: ' + this.state.user_data.udPhone}</div>
                      <div className="small text-muted"><i className="fa fa-map-pin"></i>{' Address: ' + this.state.user_data.udAddress}</div>
                    </Col>
                  </Row>
                </CardBody>
              </Card>
            </Col>
          </Row>
          <Row>
            <Col xs="12" lg="12">
              <Card>

                <CardHeader>
                  <span> </span>
                  <i className="fa fa-align-justify"></i> Products of Order #{orderID}
                  {this.state.order.ohStatus === 'CONFIRMED' ? <Button className="float-right" color="primary" onClick={this.makePDF}><i className="cui-envelope-letter" ></i> Invoice</Button> : null}

                </CardHeader>

                <CardBody>
                  <div className="col-sm">
                  </div>
                  <div className="col-sm">
                  </div>
                  <div className="col-sm">

                  </div>
                  <Table className="table table-hover">
                    <thead>
                      <tr>
                        <th scope="col"  style={{ cursor: 'pointer' }}>Name</th>
                        <th scope="col"  style={{ cursor: 'pointer' }}>Quantity</th>

                        <th scope='col'  style={{ cursor: 'pointer' }}>Total before discount</th>
                        <th scope="col"  style={{ cursor: 'pointer' }}>Discount</th>
                        <th scope='col'  style={{ cursor: 'pointer' }}>Total after discount</th>

                      </tr>
                    </thead>
                    <tbody>
                      {this.state.data.map(function (item, key) {
                        item.ProductTotalBfDisc = item.pPrice * item.opQuant
                        item.ProductTotal = item.pPrice * item.opQuant * (1 - item.opDisc)


                        return (
                          <tr key={key} className={`text-dark`}>
                            <td>{item.pName}</td>
                            <td>{item.opQuant}</td>

                            <td>{new Intl.NumberFormat('en-GB', {
                              style: 'currency',
                              currency: 'EUR',
                              minimumFractionDigits: 2,
                              maximumFractionDigits: 2
                            }).format(item.ProductTotalBfDisc)}</td>
                            <td>{<InputGroup><Input max={100} min={0} precision={2} type="number" placeholder={Math.floor((item.opDisc) * 100)} onChange={(e) => { this.onDiscountChange(e, key) }}></Input> <InputGroupAddon addonType="append">
                              <InputGroupText>%</InputGroupText></InputGroupAddon></InputGroup>}</td>
                            <td>{new Intl.NumberFormat('en-GB', {
                              style: 'currency',
                              currency: 'EUR',
                              minimumFractionDigits: 2,
                              maximumFractionDigits: 2
                            }).format(item.ProductTotal)}</td>
                          </tr>
                        )
                      }.bind(this))}

                    </tbody>
                    <tfoot>
                      <tr>
                        <td></td>
                        <td></td>
                        <td></td>

                        <td><b>Total:</b></td>
                        {}
                        <td>{new Intl.NumberFormat('en-GB', {
                          style: 'currency',
                          currency: 'EUR',
                          minimumFractionDigits: 2,
                          maximumFractionDigits: 2
                        }).format(this.state.order.ohTotal)
                        }</td>

                      </tr>

                      {this.state.order.ohStatus === 'PENDING' ? <tr><td></td><td></td><td></td><td><Button onClick={this.togglec} className="btn-ghost-success">Confirm Order</Button></td><td><Button onClick={this.toggle} className="btn-ghost-danger">Cancel Order</Button></td></tr> : null}

                    </tfoot>
                  </Table>
                  <Modal className="modal-danger" isOpen={this.state.modal} toggle={this.toggle}>
                    <ModalHeader toggle={this.toggle}>Cancel Order</ModalHeader>
                    <ModalBody>
                      Are you sure you want to cancel this order?
                  </ModalBody>
                    <ModalFooter>
                      <Button color="danger" onClick={this.saveData}>Yes</Button>{' '}
                      <Button color="secondary" onClick={this.toggle}>Go Back</Button>
                    </ModalFooter>
                  </Modal>

                  <Modal className="modal-success" isOpen={this.state.modalc} toggle={this.togglec}>
                    <ModalHeader toggle={this.togglec}>Confirm Order</ModalHeader>
                    <ModalBody>
                      Are you sure you want to confirm this order?
                  </ModalBody>
                    <ModalFooter>
                      <Button color="success" onClick={this.confimOrder}>Yes</Button>{' '}
                      <Button color="secondary" onClick={this.togglec}>Go Back</Button>
                    </ModalFooter>
                  </Modal>
                </CardBody>
              </Card>
            </Col>
          </Row>
          <Modal className="modal-info" isOpen={this.state.modalEXP} toggle={this.toggleEXP}>
            <ModalHeader toggle={this.toggleEXP}>Session Expired</ModalHeader>
            <ModalBody>
              Your session has expired. Please log in to continue.
          </ModalBody>
            <ModalFooter>
              <Button color="info" onClick={this.handleChangesEXP}>Yes</Button>
            </ModalFooter>
          </Modal>
        </div>

      );
    }
  }
}

export default DistOrderProducts;
