import React from 'react';
import ReactDOM from 'react-dom';
import DistOrderProducts from './DistOrderProducts';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<DistOrderProducts />, div);
  ReactDOM.unmountComponentAtNode(div);
});
