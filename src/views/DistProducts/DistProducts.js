import React, { Component } from 'react';
import { Card, CardBody, CardHeader, Col, Row, Table, Button, Input, InputGroup, InputGroupAddon, InputGroupText, Pagination, PaginationItem, PaginationLink, Modal, ModalHeader, ModalFooter, ModalBody } from 'reactstrap';
import rp from 'request-promise';
import Cookies from 'universal-cookie';
import { ToastContainer, toast } from 'react-toastify';
// eslint-disable-next-line
import toastifyFont from 'react-toastify/dist/ReactToastify.css';

const cookies = new Cookies();

var data = []
var userID = 0
var orderID = 0
var token = ''

localStorage.__proto__.addItem = function (item) {
  localStorage.setItem('data', item)
  var event = new Event('itemInserted');
  window.dispatchEvent(event);
}




class DistProducts extends Component {

  constructor(props) {
    super(props)
    this.state = {
      data: null,
      sort: '',
      distributors: [],
      dist: "ALL",
      prod_count: [],
      current_page: 1,
      isChecked: true,
      modalEXP: false
    }
    this.compareBy.bind(this);
    //this.sortBy.bind(this);
    this.thisrch = this.thisrch.bind(this);
    this.removeFilter = this.removeFilter.bind(this);
    this.updatepPrice = this.updatepPrice.bind(this);
    this.onChangePrice = this.onChangePrice.bind(this);
    this.toggleSuccess = this.toggleSuccess.bind(this);
    this.toggleEXP = this.toggleEXP.bind(this);
    this.handleChangesEXP = this.handleChangesEXP.bind(this)

    if (localStorage.getItem("data") === null) {
      localStorage.setItem("data", JSON.stringify([]))
    }
    else {
      this.basket = JSON.parse(localStorage.getItem("data"))
    }

  }

  toggleEXP() {
    this.setState({
      modalEXP: !this.state.modal
    });
  }

  handleChangesEXP() {
    this.toggleEXP()
    this.props.history.push('../Login')
    window.location.reload();

  }
  compareBy(key) {
    return function (a, b) {
      if (a[key] < b[key]) return -1;
      if (a[key] > b[key]) return 1;
      return 0;
    };
  }

  thisrch(e) {
    this.setState({ current_page: 1 })
    var string = e.target.value.toLowerCase().trim()
    var splitString = string.split(" ")
    var splitted = ""
    var data = []
    if(splitString.length === 1){
      data = this.props.location.state.data.filter((prod) => {
        return prod.pName.toLowerCase().match(splitString)
        //return value.test(prod.pName.toLowerCase())
      })
    }
    else if(splitString.length > 1){
      splitted = "(?=.*" + splitString.join(")(?=.*") + ")"
      var value = new RegExp(splitted)
      data = this.props.location.state.data.filter((prod) => {
        return prod.pName.toLowerCase().match(value)
        //return value.test(prod.pName.toLowerCase())
    })
  }
    this.setState({ data: data })
  }

  toggleSuccess() {
    this.setState({
      success: !this.state.success,
    });
  }

  onChangePrice(e, item) {
    let arrayCopy = [...this.state.data]

    var price = 0
    if (parseInt(e.target.value) <= parseInt("0")) {
      e.target.value = 0
      price = 0
    }
    else {
      price = e.target.value
    }

    arrayCopy.forEach((i) => {
      if (i.pID === item.pID) {
        if(i.hasChanged){
          if(i.initPrice===price){
            i.hasChanged = false
          }
        }
        else{
          i.initPrice = i.pPrice
          i.hasChanged = true
        }
        i.pPrice = price
      }
    })

    this.setState({ data: arrayCopy })

  }

  resetPrice(e, item, key){
    let arrayCopy = [...this.state.data]
    let input = document.getElementById(`input-${key}`)

    arrayCopy.forEach((i) => {
      if (i.pID === item.pID) {
        i.pPrice = i.initPrice
        input.value = ""
        input.placeholder = i.initPrice
        this.setState({ data: arrayCopy })
        e.target.value = i.initPrice
        this.onChangePrice(e,i)

      }
    })
  }

  updatepPrice(item) {
    var token = ''

    if (localStorage.getItem("id_token")) {
      token = localStorage.getItem("id_token")
    }

    if (cookies.get('id_token')) {
      token = cookies.get('id_token')
    }

    var updateProduct = {
      uri: "https://sng-heroku-api.herokuapp.com/api/product",
      headers: {
        data: JSON.stringify([{ "pPrice": item.pPrice }, { "pID": item.pID }]),
        Authorization: 'Bearer ' + token
      },
      json: true,
      method: "PUT"
    }
    rp(updateProduct).then(

      (res) => {
        toast.success(item.pName + "'s price has been updated!", {
          position: toast.POSITION.BOTTOM_CENTER,
          autoClose: 3000
        });
        item.hasChanged = false
        this.getData()
        return res
      }).catch((error) => {
        if(error.statusCode===403)
          this.toggleEXP()
      })

      

  }

  updatepStock(e, item) {
    let arrayCopy = [...this.state.data]

    if (item.pStock === 0) {
      item.pStock = 1;
    } else {
      item.pStock = 0;
    }

    var updateProduct = {
      uri: "https://sng-heroku-api.herokuapp.com/api/product",
      headers: {
        data: JSON.stringify([{ "pStock": item.pStock }, { "pID": item.pID }]),
        Authorization: 'Bearer ' + token
      },
      json: true,
      method: "PUT"
    }
    rp(updateProduct).then(

      (res) => {

        arrayCopy.forEach((i) => {
          if (i.pID === item.pID) {
            i.pStock = item.pStock
          }
        })

        this.setState({ data: arrayCopy })

        return res
      }).catch((error) => {
        if(error.statusCode===403)
          this.toggleEXP()
      })



  }


  // sortBy(key) {
  //   let arrayCopy = [...this.state.data];

  //   if (this.state.sort === '' || this.state.sort === 'desc') {
  //     this.setState({ sort: 'asc' })
  //     arrayCopy.sort(this.compareBy(key));
  //   }

  //   if (this.state.sort === 'asc') {
  //     this.setState({ sort: 'desc' })
  //     arrayCopy.reverse(this.compareBy(key));
  //   }

  //   this.setState({ data: arrayCopy });
  // }

  // sendData = (item) => {
  //   localStorage.setItem(item)
  // }



  getData = () => {
      var products_by_dist = {
        uri: "https://sng-heroku-api.herokuapp.com/api/dist_products",
        headers: {
          
          Authorization: 'Bearer ' + token
        },
        json: true
      }
      rp(products_by_dist).then(

        (prod) => {

          data = prod.data
          this.props.location.state = { data: data };
          this.setState({ data: data, dist: data[0].uFname })

          return prod

        }
      ).catch((error) => {
        if(error.statusCode===403)
          this.toggleEXP()
      }).then(

        () => {
          var distributors = {
            uri: "https://sng-heroku-api.herokuapp.com/api/distributors",
            headers: {
              Authorization: 'Bearer ' + token
            },
            json: true
          }
          rp(distributors).then(

            (dist) => {
              this.setState({ distributors: dist.data })

              return dist

            }).catch((error) => {
              if(error.statusCode===403)
                this.toggleEXP()
            }).then(

              () => {
                var prod_count = {
                  uri: "https://sng-heroku-api.herokuapp.com/api/count_prod",
                  headers: {
                    Authorization: 'Bearer ' + token
                  },
                  json: true
                }
                rp(prod_count).then(

                  (count) => {

                    this.setState({ prod_count: count.data[0].cp })

                    return count

                  }).catch((error) => {
                    if(error.statusCode===403)
                      this.toggleEXP()
                  })
              }).catch((error) => {
                if(error.statusCode===403)
                  this.toggleEXP()
              })
              return null
        }).catch((error) => {
          if(error.statusCode===403)
            this.toggleEXP()
        })
      return true;
    // }

  }

  removeFilter() {

    var data = []
    var products = {
      uri: "https://sng-heroku-api.herokuapp.com/api/products",
      headers: {
        Authorization: 'Bearer ' + token
      },
      json: true
    }
    rp(products).then(

      (prod) => {

        data = prod.data
        this.props.location.state = { data: data };
        this.setState({ data: data, dist: "" })

        return prod

      }
    ).catch((error) => {
      this.toggleEXP()
    }).then(

      () => {

        var distributors = {
          uri: "https://sng-heroku-api.herokuapp.com/api/distributors",
          headers: {
            Authorization: 'Bearer ' + token
          },
          json: true
        }
        rp(distributors).then(

          (dist) => {
            this.setState({ distributors: dist.data })

            return dist
            
          }).catch((error) => {
            this.toggleEXP()
          })
        return null

      }).catch((error) => {
        if(error.statusCode===403)
                    this.toggleEXP()
      })
    return null

  }

  componentWillMount() {
    if (localStorage.getItem("id_token")) {
      token = localStorage.getItem("id_token")
    }
    
    if (cookies.get('id_token')) {
      token = cookies.get('id_token')
    }

    if (cookies.get('id_token') === undefined && localStorage.getItem('id_token') === null) {
      this.props.history.push('../Login')

    }

    this.setState({ data: data, userID: userID, orderID: orderID, token: token })
    this.getData();
  }


  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
    var data = this.props.location.state.data.filter((prod) => {

      return prod.dName.includes(e.target.value)



    })
    this.setState({ data: data })
  };

  render() {
    if (localStorage.getItem("userType") === '0' && (cookies.get('id_token') !== undefined || localStorage.getItem('id_token') !== null)) {
      this.props.history.replace('/404')
      return null
    }
    else {

      return (

        <div className="animated fadeIn">
          <ToastContainer />

          <Row>
            <Col xs="12" lg="12">
              <Card>
                <CardHeader>
                  <i className="fa fa-align-justify"></i> Products <Input className="col-sm-2 float-right" placeholder="Search" onChange={(e) => { this.thisrch(e) }} />



                </CardHeader>
                <CardBody>
                  <Table className="table table-hover">
                    <thead>
                      <tr>
                        <th scope="col"  style={{ cursor: 'pointer' }}>Product ID</th>
                        <th scope="col"  style={{ cursor: 'pointer' }}>Name</th>
                        <th scope="col"  style={{ cursor: 'pointer' }}>Price</th>
                        <th scope='col'  style={{ cursor: 'pointer' }}>In Stock</th>
                        <th></th>

                      </tr>
                    </thead>
                    <tbody>
                      {this.state.data.map(function (item, key) {
                        if (key < this.state.current_page * 10 && key >= this.state.current_page * 10 - 10) {
                          return (

                            <tr key={key} className={this.status}>
                              <td>{item.pID}</td>
                              <td><div>{item.pName}</div><div className="small text-muted"><span>Description</span> : {item.pDesc}</div></td>

                              <td><InputGroup className="mb-3">
                                <InputGroupAddon addonType="prepend">
                                  <InputGroupText><i className="">€</i></InputGroupText>
                                </InputGroupAddon>
                                <Input min="0.01" placeholder={item.pPrice} id={`input-${key}`} type="number" onChange={(e) => { this.onChangePrice(e, item) }} />
                              </InputGroup> </td>
                              <td className="text-center">
                                {item.pStock === 1 ? (
                                  <Input className="text-center" type="checkbox" checked={this.state.isChecked} onChange={(e) => { this.updatepStock(e, item) }} />

                                ) : (

                                    <Input className="text-center" type="checkbox" checked={!this.state.isChecked} onChange={(e) => { this.updatepStock(e, item) }} />


                                  )
                                }

                              </td>
                              <td>
                              {
                                item.hasChanged === true ? (
                                  <div>
                                    <Button outline color='success' onClick={() => { this.updatepPrice(item) }}>Update Product</Button>
                                    {" "}
                                    <Button outline color='warning' onClick={(e) => { this.resetPrice(e,item, key) }}>Reset inital value</Button>
                                  </div>
                                ) : (false)
                              }
                              </td>
                            </tr>
                          )
                        }
                      }.bind(this))}

                    </tbody>
                  </Table>

                  <Pagination>
                    <PaginationItem>
                      <PaginationLink tag="button" onClick={() => { this.setState({ current_page: 1 }) }}>First</PaginationLink>
                    </PaginationItem>
                    <PaginationItem>
                      <PaginationLink previous tag="button" onClick={() => { if (this.state.current_page * 10 - 10 > 0) { this.setState({ current_page: (this.state.current_page - 1) }) } }}></PaginationLink>
                    </PaginationItem>
                    {
                      this.state.data.map(function (item, key) {
                        key += 1
                        if (key % 10 === 1) {
                          let temp_page = Math.trunc((key / 10) + 1)
                          let last_page = Math.trunc((this.state.data.length / 10) + 1)
                          // Dot system
                          if (last_page > 10) {
                            // Inner current page
                            if (this.state.current_page >= 6 && last_page - this.state.current_page >= 6) {
                              if (temp_page < 3) {
                                return (
                                  <PaginationItem active={this.state.current_page === temp_page}>
                                    <PaginationLink onClick={() => { this.setState({ current_page: temp_page }) }} tag="button">{temp_page}</PaginationLink>
                                  </PaginationItem>
                                )
                              }
                              else if (temp_page === 3) {
                                return (
                                  <PaginationItem disabled>
                                    <PaginationLink tag="button">...</PaginationLink>
                                  </PaginationItem>
                                )
                              } else if (temp_page - this.state.current_page >= -2 && temp_page - this.state.current_page <= 2) {
                                return (
                                  <PaginationItem active={this.state.current_page === temp_page}>
                                    <PaginationLink onClick={() => { this.setState({ current_page: temp_page }) }} tag="button">{temp_page}</PaginationLink>
                                  </PaginationItem>
                                )
                              }

                              if (last_page - temp_page < 2) {
                                return (
                                  <PaginationItem active={this.state.current_page === temp_page}>
                                    <PaginationLink onClick={() => { this.setState({ current_page: temp_page }) }} tag="button">{temp_page}</PaginationLink>
                                  </PaginationItem>
                                )
                              }
                              else if (last_page - temp_page === 2) {
                                return (
                                  <PaginationItem disabled>
                                    <PaginationLink tag="button">...</PaginationLink>
                                  </PaginationItem>
                                )
                              }

                            }
                            // lower current page
                            else if (this.state.current_page <= 6) {
                              if (last_page - temp_page < 2) {
                                return (
                                  <PaginationItem key={key} active={this.state.current_page === temp_page}>
                                    <PaginationLink onClick={() => { this.setState({ current_page: temp_page }) }} tag="button">{temp_page}</PaginationLink>
                                  </PaginationItem>
                                )
                              }
                              else if (last_page - temp_page === 2) {
                                return (
                                  <PaginationItem key={key} disabled>
                                    <PaginationLink tag="button">...</PaginationLink>
                                  </PaginationItem>
                                )
                              } else if (temp_page <= 6) {
                                return (
                                  <PaginationItem key={key} active={this.state.current_page === temp_page}>
                                    <PaginationLink onClick={() => { this.setState({ current_page: temp_page }) }} tag="button">{temp_page}</PaginationLink>
                                  </PaginationItem>
                                )
                              }
                            }
                            // upper current page
                            else if (last_page - this.state.current_page <= 6) {
                              if (temp_page < 3) {
                                return (
                                  <PaginationItem active={this.state.current_page === temp_page}>
                                    <PaginationLink onClick={() => { this.setState({ current_page: temp_page }) }} tag="button">{temp_page}</PaginationLink>
                                  </PaginationItem>
                                )
                              }
                              else if (temp_page === 3) {
                                return (
                                  <PaginationItem disabled>
                                    <PaginationLink tag="button">...</PaginationLink>
                                  </PaginationItem>
                                )
                              } else if (last_page - temp_page <= 6) {
                                return (
                                  <PaginationItem active={this.state.current_page === temp_page}>
                                    <PaginationLink onClick={() => { this.setState({ current_page: temp_page }) }} tag="button">{temp_page}</PaginationLink>
                                  </PaginationItem>
                                )
                              }
                            }
                          } else {
                            return (
                              <PaginationItem active={this.state.current_page === temp_page}>
                                <PaginationLink onClick={() => { this.setState({ current_page: temp_page }) }} tag="button">{temp_page}</PaginationLink>
                              </PaginationItem>
                            )
                          }
                        }
                      }.bind(this)
                      )
                    }

                    <PaginationItem>
                      <PaginationLink next tag="button" onClick={() => { if (this.state.current_page * 10 + 1 < this.state.data.length) { this.setState({ current_page: (this.state.current_page + 1) }) } }}></PaginationLink>
                    </PaginationItem>

                    <PaginationItem>
                      <PaginationLink tag="button" onClick={() => { if (this.state.data.length % 10 > 0) { this.setState({ current_page: Math.trunc((this.state.data.length / 10) + 1) }) } else { this.setState({ current_page: this.state.data.length / 10 }) } }}>Last</PaginationLink>
                    </PaginationItem>
                  </Pagination>
                </CardBody>
              </Card>
            </Col>
          </Row>
          <Modal className="modal-info" isOpen={this.state.modalEXP} toggle={this.toggleEXP}>
            <ModalHeader toggle={this.toggleEXP}>Session Expired</ModalHeader>
            <ModalBody>
              Your session has expired. Please log in to continue. 
          </ModalBody>
            <ModalFooter>
              <Button color="info" onClick={this.handleChangesEXP}>Yes</Button>
            </ModalFooter>
          </Modal>
        </div>

      );
    }
  }
}

export default DistProducts;
