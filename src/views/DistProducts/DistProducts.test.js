import React from 'react';
import ReactDOM from 'react-dom';
import DistProducts from './DistProducts';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<DistProducts />, div);
  ReactDOM.unmountComponentAtNode(div);
});
