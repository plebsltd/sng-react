import React, { Component } from 'react';
import { Hero, Checklist } from 'react-landing-page';
import {
  Nav, NavLink, NavItem, Button, Row, Col, Card, CardTitle, CardBody, CardHeader, CardText
} from 'reactstrap';
//import useTypingEffect from 'use-typing-effect';

class Landing extends Component {
  render() {
    return (
      <div>

        <Nav nav style={{ 'display': 'inline-block', 'width': '100%', 'background-color': '#010204' }}>
          <a className="navbar-brand" href="/" style={{ 'margin-left': '15px', 'margin-top': '5px' }}>
            <img class="navbar-brand-full" src="../../assets/img/logo.png" height="30" alt="SnG Logo" />
          </a>

          <NavItem className="float-right" style={{ 'margin-right': '15px' }}>
            <NavLink href="/#/login">
              <Button size="" color="outline-primary" className="btn-pill btn-lg">Login</Button>
            </NavLink>
          </NavItem>
        </Nav>
        <Hero color="white" backgroundImage="../../assets/img/skato.png">
          <img src="../../assets/img/logo2.png" height="170" alt="SnG Logo" />
          <h1 style={{ '-webkit-text-stroke': '1px black' }}>Making the acquirement and distribution of products faster</h1>
        </Hero>
        <br></br>
        <Row>
          <Col lg="12" >
            <Card>
              <CardHeader>
                <h1 className="text-center">About Supply and Go</h1>
              </CardHeader>
              <CardBody>
                <p className="lead" >Is your company still using  phone calls for ordering your products? If yes give us a call try our new product Supply and go!! </p>
                <p className="lead">Make orders easy for both suppliers and distributors! Supply and go will save time and reduced phone call and paperwork. </p>
                <p className="lead"> Supply and go will make you more productive for organising your orders. It's easy to use with receipts of your orders</p>
                <p className="lead"> More than 60 distributors and hundreds of products</p>
                <Card body>
                  <Checklist fontSize={1} checkmark={"☑️"} children={[
                    'Easy to use',
                    'Intuitive design',
                    'Made from a team of plebs with a lot of passion'
                  ]} />
                </Card>
              </CardBody>
            </Card>
          </Col>
        </Row>

        <Row>
          <Col lg="12" >
            <Card>
              <CardHeader>
                <h1 className="text-center"> Features of Supply and Go</h1>
              </CardHeader>
              <CardBody>
                <Row>
                  <Col sm="6" lg='6' xl="6">
                    <Card body>
                      <CardTitle><i class="cui-speedometer h3"> </i>High Performance</CardTitle>
                      <CardText>With supporting text below as a natural lead-in to additional content.</CardText>
                    </Card>
                  </Col>
                  <Col sm="6" lg='6'>
                    <Card body>
                      <CardTitle><i class="cui-user h3"> </i>User Friendly</CardTitle>
                      <CardText>With supporting text below as a natural lead-in to additional content.</CardText>
                    </Card>
                  </Col>
                </Row>
                <Row>
                  <Col sm="6">
                    <Card body>
                      <CardTitle><i class="cui-lock-locked h3"> </i>100% Secure</CardTitle>
                      <CardText>With supporting text below as a natural lead-in to additional content.</CardText>
                    </Card>
                  </Col>
                  <Col sm="6">
                    <Card body>
                      <CardTitle><i class="cui-settings h2"></i>Unlimited Features</CardTitle>
                      <CardText>With supporting text below as a natural lead-in to additional content.</CardText>
                    </Card>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>

        <br></br>

        <Row>
          <Col lg='12'>
            <Card>
              <CardHeader>
                <span> </span>
                <h1 className="text-center">Get in touch</h1>
              </CardHeader>
              <CardBody lg="12">
                <Row>
                  <Col lg="12">
                    <CardText className="text-center">
                      <p className="lead">What can you achieve with Supply and Go? Get in touch to speak with the team who are transforming hospitality experiences.
                        <br></br>
                        <br></br>
                        For all media enquiries, you can reach our team at team@supplyandgo.com
                      </p>
                      <br></br>
                      <a href="mailto:team@supplyandgo.com">
                        <Button color="outline-primary" className="btn btn-lg btn-pill" >Contact Us</Button>
                      </a>
                    </CardText>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>

    );
  }
}

export default Landing;
