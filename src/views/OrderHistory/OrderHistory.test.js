import React from 'react';
import ReactDOM from 'react-dom';
import OrderHistory from './OrderHistory';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<OrderHistory />, div);
  ReactDOM.unmountComponentAtNode(div);
});
