import React, { Component } from 'react';
import { Card, CardBody, CardTitle, CardHeader, Col, Row, Table, Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import rp from 'request-promise';
import { Image} from 'cloudinary-react';
import pdfMake from 'pdfmake/build/pdfmake.js';
import pdfFonts from 'pdfmake/build/vfs_fonts';
import { ToastContainer, toast } from 'react-toastify';
// eslint-disable-next-line
import toastifyFont from 'react-toastify/dist/ReactToastify.css';
import Cookies from 'universal-cookie';

const imageToURI = require('image-to-data-uri')

const cookies = new Cookies();
var data = []
var orderID = 0

var token = '';


pdfMake.vfs = pdfFonts.pdfMake.vfs;



localStorage.__proto__.addItem = function (item) {

  localStorage.setItem('data', item)

  var event = new Event('itemInserted');
  window.dispatchEvent(event);

}


class OrderProducts extends Component {

  constructor(props) {
    super(props)
    this.state = {
      data: null,
      sort: '',
      orderTotal: 0,
      OH_Confirmation: "",
      modal: false,
      modalOA: false,
      modalEXP: false,
      statemodalOAB: false,
      order_OutOfStock: false
    }
    this.toggle = this.toggle.bind(this);
    this.compareBy.bind(this);
    //this.sortBy.bind(this);
    this.saveData = this.saveData.bind(this);
    this.emailSender = this.emailSender.bind(this);
    this.handleChanges = this.handleChanges.bind(this);
    this.makePDF = this.makePDF.bind(this);
    this.toggleOA = this.toggleOA.bind(this);
    this.orderAgain = this.orderAgain.bind(this);
    this.toggleEXP = this.toggleEXP.bind(this);
    this.handleChangesEXP = this.handleChangesEXP.bind(this)
    this.toggleOABasket = this.toggleOABasket.bind(this);
    this.clearBasketOA = this.clearBasketOA.bind(this);

    if (localStorage.getItem("data") === null) {
      localStorage.setItem("data", JSON.stringify([]))
    }
    else {
      this.basket = JSON.parse(localStorage.getItem("data"))
    }


  }

  toggleEXP() {
    this.setState({
      modalEXP: !this.state.modal
    });
  }

  handleChangesEXP() {
    this.toggleEXP()
    this.props.history.push('../Login')
    window.location.reload();

  }


  compareBy(key) {
    return function (a, b) {
      if (a[key] < b[key]) return -1;
      if (a[key] > b[key]) return 1;
      return 0;
    };
  }

  // sortBy(key) {
  //   let arrayCopy = [...this.state.data];

  //   if (this.state.sort === '' || this.state.sort === 'desc') {
  //     this.setState({ sort: 'asc' })
  //     arrayCopy.sort(this.compareBy(key));
  //   }

  //   if (this.state.sort === 'asc') {
  //     this.setState({ sort: 'desc' })
  //     arrayCopy.reverse(this.compareBy(key));
  //   }

  //   this.setState({ data: arrayCopy });
  // }

  getData = () => {

    var data = []
    var order_products = {
      uri: "https://sng-heroku-api.herokuapp.com/api/order_products_by_order",
      headers: {
        data: JSON.stringify([{ "op.opOHID": this.props.match.params.orderid }]),
        Authorization: 'Bearer ' + token
      },
      json: true
    }
    rp(order_products).then(

      (ord_prod) => {

        data = ord_prod.data
        var total = 0;
        var counter = 0;
        var isOutOfStock;
        ord_prod.data.forEach((element) => {
          if (element.pStock === 0) {
            counter += 1
          }

          total += (element.opQuant * element.pPrice * element.opDisc)
        })

        if (ord_prod.data.length === counter) {
          isOutOfStock = true
        }
        else {
          isOutOfStock = false
        }

        this.props.location.state = { data: data, orderID: orderID, token: token, orderTotal: total }
        this.setState({ data: data, orderID: orderID, token: token, orderTotal: total, order_OutOfStock: isOutOfStock })

        return ord_prod

      }
    ).catch((error) => {
      if (error.statusCode === 403)
        this.toggleEXP()
    }).then(

      () => {
        var order = {
          uri: "https://sng-heroku-api.herokuapp.com/api/order",
          headers: {
            data: JSON.stringify([{ "ohID": this.props.match.params.orderid }]),
            Authorization: 'Bearer ' + token
          },
          json: true
        }
        rp(order).then(

          (ord) => {
            data = ord.data[0]

            this.setState({ order: data })

            return ord

          }).catch((error) => {
            if (error.statusCode === 403)
              this.toggleEXP()
          })

        return null

      }).catch((error) => {
        if (error.statusCode === 403)
          this.toggleEXP()
      }).then(

        () => {

          var userDetails = {
            uri: "https://sng-heroku-api.herokuapp.com/api/user_details",
            headers: {
              Authorization: 'Bearer ' + token
            },
            json: true
          }
          rp(userDetails).then(

            (ud) => {
              this.setState({ userDetails: ud.data[0] })

              return ud

            }).catch((error) => {
              if (error.statusCode === 403)
                this.toggleEXP()
            })
          return null
        }).catch((error) => {
          if (error.statusCode === 403)
            this.toggleEXP()
        })
    return true
  }


  componentWillMount() {
    if (localStorage.getItem("id_token")) {
      token = localStorage.getItem("id_token")
    }

    if (cookies.get('id_token')) {
      token = cookies.get('id_token')
    }

    if (cookies.get('id_token') === undefined && localStorage.getItem('id_token') === null) {
      this.props.history.push('../Login')

    }
    this.setState({ data: data, orderID: orderID, token: token, order: { ohStatus: "" } })
    this.getData();
  }

  toggle() {
    this.setState({
      modal: !this.state.modal
    });
  }

  toggleOA() {
    this.basket = JSON.parse(localStorage.getItem("data"));
    if (this.basket.length > 0) {
      this.toggleOABasket()
    }
    else {
      this.setState({
        modalOA: !this.state.modalOA
      })

    }
  }

  toggleOABasket() {
    this.setState({
      statemodalOAB: !this.state.statemodalOAB
    })
  }

  clearBasketOA() {
    this.toggleOABasket();
    localStorage.removeItem("data");
    localStorage.setItem("data", JSON.stringify([]));
    var event = new Event('basketDeleted');
    window.dispatchEvent(event);

    this.basket = JSON.parse(localStorage.getItem("data"));

    let arrayCopy = [...this.state.data];

    arrayCopy.forEach((e) => {
      if (e.pStock !== 0) {
        e.quantity = e.opQuant;
        this.basket.push(e)
      }
    })

    localStorage.addItem(JSON.stringify(this.basket));

    toast.info("A new order has been added to your basket!", {
      position: toast.POSITION.BOTTOM_CENTER,
      autoClose: 3000
    });
  }

  orderAgain() {
    this.toggleOA();
    this.basket = JSON.parse(localStorage.getItem("data"));

    let arrayCopy = [...this.state.data];

    var prod_found = false;

    arrayCopy.forEach((e) => {
      e.quantity = e.opQuant;
      this.basket.forEach((elem) => {
        if ((e.pID === elem.pID) && (e.pStock !== 0)) {
          prod_found = true
          elem.quantity += e.quantity
        }

      })

      if ((!prod_found) && (e.pStock !== 0)) {
        this.basket.push(e);
      }

      prod_found = false

      if (e.pStock === 0) {
        toast.error(`${e.pName} is out of stock !`, {
          position: toast.POSITION.BOTTOM_CENTER,
          autoClose: 5000
        });
      }

    });

    localStorage.addItem(JSON.stringify(this.basket));

    toast.info("The order has been added to your basket!", {
      position: toast.POSITION.BOTTOM_CENTER,
      autoClose: 3000
    });

  }


  saveData() {

    var data = [{ ohStatus: "CANCELED" }, { ohID: this.state.order.ohID }]
    var order = {
      uri: "https://sng-heroku-api.herokuapp.com/api/orders",
      headers: {
        data: JSON.stringify(data),
        Authorization: 'Bearer ' + token
      },
      json: true,
      method: 'PUT'
    }

    rp(order).then(

      (res) => {

        var order = this.state.order
        order.ohStatus = "CANCELED"

        this.setState({ order: order })

        toast.error("Your order has been canceled.", {
          position: toast.POSITION.BOTTOM_CENTER,
          autoClose: 3000

        });

        // this.toggle();

        return res

      }).catch((error) => {
        if (error.statusCode === 403)
          this.toggleEXP()
      })


    return true
  };

  handleChanges() {
    this.toggle();
    this.saveData();
    this.emailSender();
  }


  emailSender() {

    var email = {
      uri: "https://sng-heroku-api.herokuapp.com/api/send_email",
      headers: {
        // data: JSON.stringify({order: e, uFname: name, distEmail: distEmail, sendingOrder: false}),
        data: JSON.stringify({ order: this.state.data, sendingOrder: false }),
        Authorization: 'Bearer ' + token
      },
      method: 'POST',
      json: true
    }
    rp(email).then((email) => {
      return email;
    }).catch((error) => {
      if (error.statusCode === 403)
        this.toggleEXP()
    })
    return true;
  }

  makePDF() {
    var imageUri = ""
    imageToURI(`https://res.cloudinary.com/xatzisktv/image/upload/${this.state.order.udIcon}`, function (err, uri) {
      if (!err) {
        console.log(uri)
        imageUri = uri
        var pdf_data = [[
          { text: 'Product Name', style: 'tableHeader' },
          { text: 'Price', style: 'tableHeader' },
          { text: 'Quantity', style: 'tableHeader' },
          { text: 'Total before Discount', style: 'tableHeader' },
          { text: 'Discount', style: 'tableHeader' },
          { text: 'VAT', style: 'tableHeader' },
          { text: 'Total after Discount', style: 'tableHeader' }]]
        this.state.data.forEach((e) => {
          var subtotal = e.pPrice * e.opQuant;
          var subtotaladisc = e.pPrice * e.opQuant * (1 - e.opDisc);

          pdf_data.push([e.pName, "€" + e.pPrice.toFixed(2), "" + e.opQuant, "€" + subtotal.toFixed(2), + e.opDisc * 100 + "%", "" + e.pVAT * 100 + "%", "€" + subtotaladisc.toFixed(2)])
        })
        var totalaftervat = this.state.order.ohTotal.toFixed(2) * 1.19
        var pdf_data_total = []

        pdf_data_total.push([{ text: 'Total after Discount', bold: true }, "€" + this.state.order.ohTotal.toFixed(2)])
        pdf_data_total.push([{ text: 'Total after VAT', bold: true }, { text: "€" + totalaftervat.toFixed(2), bold: true }])

        var docDefinition = {
          footer: {
            margin: [30,0,0,0],
            columns: [
              {
                text:[
                  {text:"SupplyAndGo © 2019 SupplyAndGo.\n",style:"small"},
                  {text:"For any enquiries please contact us at: info@supplyandgo.com",style:"small"}
                ]
              }
            ]
          },
          content: [
            {
              alignment: 'justify',
              columns: [
                {
                  text: [{
                    text: 'Invoice\n',
                    style: 'header'
                  }, {
                    text: 'Powered by Supply and Go\n\n\n',
                    style: 'small'
                  }]

                },
                {

                  width: 100,
                  image: imageUri
                }
                ,
              ]
            },
            {
              alignment: 'justify',
              columns: [
                {
                  text: [{ text: `From`, bold: true }, `\n${this.state.order.uFname}\n${this.state.order.udAddress}\n${this.state.order.udPhone}`],
                  style: 'header2'
                },
                {
                  text: [{ text: `To`, bold: true }, `\n${this.state.userDetails.uFname}\n${this.state.userDetails.udAddress}\n${this.state.userDetails.udPhone}`],
                  style: 'header2'
                }
              ]
            },
            '\n',
            {
              alignment: 'justify',
              columns: [
                {
                  text: `Order Number: ${this.state.order.ohID}\nDate: ${new Date(this.state.order.ohTimestamp).toUTCString().slice(0, 16)}`
                }
              ]
            },
            '\n',
            {
              table: {
                widths: [170, '*', 48, 50, 48, 30, 60],
                headerRows: 1,
                body:
                  pdf_data

              }
            },
            {
              table: {
                widths: ['*', 60],
                body:
                  pdf_data_total

              }
            },
            '\n\n\n',
            {
              alignment: 'left',
              columns: [

                {
                  width: 390,
                  text: `_______________________\n${this.state.order.uFname}`
                },
                { text: `_______________________\n${this.state.userDetails.uFname}` }
              ]
            }

          ],
          styles: {
            header: {
              fontSize: 18,
              bold: true
            },
            header2: {
              fontSize: 13,
              italics: true,
              margin: [0, 0, 40, 0]
            },
            tableHeader: {
              fontSize: 12,
              bold: true,
              fillColor: '#20a8d8',
              color: '#ffffff'
            },
            small: {
              fontSize: 9
            }

          }

        }
        pdfMake.createPdf(docDefinition).download('Invoice_Order_No_' + this.state.order.ohID);
      }
    }.bind(this))

  }




  render() {
    var orderID = ""



    if (this.props === undefined) {
      orderID = "Undefined"
    }
    else {
      orderID = this.props.match.params.orderid
    }
    if (localStorage.getItem("userType") === '1') {
      this.props.history.replace('/404')
      return null
    }
    else {

      return (

        <div className="animated fadeIn">
          <ToastContainer />
          <Row>
            <Col>
              <Card>
                <CardBody>
                  <Row>
                    {/* <Col xs="12" lg="1" sm='1'> */}
                    <Col md="auto">
                      <div className="text-center"><Image cloudName="xatzisktv" publicId={this.state.order.udIcon} width="80" radius="150" /></div>
                    </Col>
                    {/* <Col xs="12" lg="11" sm='11'> */}
                    <Col md="auto">
                      <CardTitle className="mb-0">Order for: {this.state.order.uFname}</CardTitle>
                      <div className="small text-muted"><i className="fa fa-envelope"></i>{' Email: '}<a href={"mailto:" + this.state.order.udEmail}>{this.state.order.udEmail}</a></div>
                      <div className="small text-muted"><i className="fa fa-phone"></i>{' Phone: ' + this.state.order.udPhone}</div>
                      <div className="small text-muted"><i className="fa fa-map-pin"></i>{' Address: ' + this.state.order.udAddress}</div>
                    </Col>
                  </Row>
                </CardBody>
              </Card>
            </Col>
          </Row>
          <Row>
            <Col xs="12" lg="12">
              <Card>

                <CardHeader>
                  <i className="fa fa-align-justify"></i> Products of Order #{orderID}
                  {this.state.order.ohStatus === 'CONFIRMED' ? <Button className="float-right" color="primary" onClick={this.makePDF}><i className="cui-envelope-letter" ></i> Invoice</Button> : null}

                </CardHeader>

                <CardBody>
                  <div className="col-sm">
                  </div>
                  <div className="col-sm">
                  </div>
                  <div className="col-sm">

                  </div>
                  <Table responsive className="table table-hover">
                    <thead>
                      <tr>
                        <th scope="col"  style={{ cursor: 'pointer' }}>Name</th>
                        <th scope="col"  style={{ cursor: 'pointer' }}>Quantity</th>
                        <th scope='col'  style={{ cursor: 'pointer' }}>Total before discount</th>
                        <th scope="col"  style={{ cursor: 'pointer' }}>Discount</th>
                        <th scope='col'  style={{ cursor: 'pointer' }}>Total after discount</th>

                      </tr>
                    </thead>
                    <tbody>
                      {this.state.data.map(function (item, key) {
                        var discount = 0

                        if (item.opDisc === 1) {
                          discount = 0;
                        }
                        else {
                          discount = item.opDisc;
                        }

                        item.ProductTotalBfDisc = item.pPrice * item.opQuant



                        item.opDisc = discount;
                        item.ProductTotal = item.pPrice * item.opQuant * (1 - item.opDisc)


                        return (
                          <tr key={key} className={`text-dark`}>
                            <td>{item.pName}</td>
                            <td>{item.opQuant}</td>
                            <td>{new Intl.NumberFormat('en-GB', {
                              style: 'currency',
                              currency: 'EUR',
                              minimumFractionDigits: 2,
                              maximumFractionDigits: 2
                            }).format(item.ProductTotalBfDisc)}</td>
                            <td>{Math.floor((discount) * 100) + "%"}</td>
                            <td>{new Intl.NumberFormat('en-GB', {
                              style: 'currency',
                              currency: 'EUR',
                              minimumFractionDigits: 2,
                              maximumFractionDigits: 2
                            }).format(item.ProductTotal)}</td>
                          </tr>
                        )
                      })}

                    </tbody>
                    <tfoot>
                      <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><b>Total:</b></td>
                        {}
                        <td>{new Intl.NumberFormat('en-GB', {
                          style: 'currency',
                          currency: 'EUR',
                          minimumFractionDigits: 2,
                          maximumFractionDigits: 2
                        }).format(this.state.order.ohTotal)
                        }</td>

                      </tr>



                    </tfoot>
                  </Table>
                  {(this.state.order.ohStatus === 'CONFIRMED' || this.state.order.ohStatus === 'CANCELED') && (this.state.order_OutOfStock === false) ? <Button onClick={this.toggleOA} className="col-sm-2 btn-ghost-success float-right"> Order Again </Button> : null}
                  <Modal className="modal-success" isOpen={this.state.modalOA} toggle={this.toggleOA}>
                    <ModalHeader toggle={this.toggleOA}>Order Again?</ModalHeader>
                    <ModalBody>
                      This order will be transfered to your basket for you to order it again. Do you want to continue?
                  </ModalBody>
                    <ModalFooter>
                      <Button color="success" onClick={this.orderAgain}>Yes</Button>{' '}
                      <Button color="secondary" onClick={this.toggleOA}>Cancel</Button>
                    </ModalFooter>
                  </Modal>

                  {this.state.order.ohStatus === 'PENDING' ? <Button onClick={this.toggle} className="col-sm-2 btn-ghost-danger float-right">Cancel Order</Button> : null}
                  <Modal className="modal-danger" isOpen={this.state.modal} toggle={this.toggle}>
                    <ModalHeader toggle={this.toggle}>Cancel Order</ModalHeader>
                    <ModalBody>
                      Are you sure you want to cancel this order?
                  </ModalBody>
                    <ModalFooter>
                      <Button color="danger" onClick={this.handleChanges}>Yes</Button>{' '}
                      <Button color="secondary" onClick={this.toggle}>Go Back</Button>
                    </ModalFooter>
                  </Modal>
                  <Modal className="modal-danger" isOpen={this.state.statemodalOAB} toggle={this.toggleOABasket}>
                    <ModalHeader toggle={this.toggleOABasket}>Basket Is Not Empty!</ModalHeader>
                    <ModalBody>
                      It appears you already have items in your basket! Do you want to keep them? If you press no they will be disregarded!
                  </ModalBody>
                    <ModalFooter>
                      <Button color="success" onClick={this.orderAgain}>Yes</Button>{' '}
                      <Button color="danger" onClick={this.clearBasketOA}>No</Button>
                    </ModalFooter>
                  </Modal>
                </CardBody>
              </Card>
            </Col>
          </Row>
          <Modal className="modal-info" isOpen={this.state.modalEXP} toggle={this.toggleEXP}>
            <ModalHeader toggle={this.toggleEXP}>Session Expired</ModalHeader>
            <ModalBody>
              Your session has expired. Please log in to continue.
          </ModalBody>
            <ModalFooter>
              <Button color="info" onClick={this.handleChangesEXP}>Yes</Button>
            </ModalFooter>
          </Modal>
        </div>

      );
    }
  }
}

export default OrderProducts;
