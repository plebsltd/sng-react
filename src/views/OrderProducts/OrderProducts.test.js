import React from 'react';
import ReactDOM from 'react-dom';
import OrderProducts from './OrderProducts';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<OrderProducts />, div);
  ReactDOM.unmountComponentAtNode(div);
});
