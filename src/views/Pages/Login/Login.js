import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row, Popover, PopoverHeader, PopoverBody, Label, FormGroup } from 'reactstrap';
import rp from 'request-promise';
import Cookies from 'universal-cookie';
const cookies = new Cookies();

class Login extends Component {

  constructor(props) {
    super(props)
    this.state = {
      redirect: false,
      popoverOpen: false,
      rememberMe: false
    }

    this.getData.bind(this)
    this.toggle = this.toggle.bind(this);
  }

  handleChange = (e) => {
    this.setState({ email: e.target.value })
  }


  canSubmit = (e) => {
    var code = (e.keyCode ? e.keyCode : e.which);
    if(code === 13) { //Enter keycode
        this.getData()
    }

  }

  handlePassChange = (e) => {
    this.setState({ password: e.target.value })
  }

  toggle = () => {
    this.setState({
      popoverOpen: !this.state.popoverOpen
    });
  }

  componentWillMount(){
    var id_local = localStorage.getItem("id_token")
    var id_cookie = cookies.get("id_token")
    let token;

    if(id_local)
      token = id_local
    if(id_cookie)
      token = id_cookie

    var check = {
      uri: "https://sng-heroku-api.herokuapp.com/api/tokenverify",
      headers: {
        Authorization: 'Bearer ' + token
      },
      method: 'GET',
      json: true
    }
    rp(check).then(
      (res) => {
        if(res.message === "VALID"){
          this.props.history.push("/Dashboard")
        }
      }
    )
  }

  getData = () => {
    var login = {
      uri: "https://sng-heroku-api.herokuapp.com/api/ulogin",
      headers: {
        email: this.state.email,
        password: this.state.password
      },
      json: true
    }
    rp(login).then(
      (res) => {       
        if(this.state.rememberMe){
          localStorage.setItem("id_token",res.token);
        }else{
          cookies.set('id_token',res.token);
        }

        this.setState({ redirect: true })
        localStorage.setItem("userType", res.userType);

        if(localStorage.getItem("data") === null){
          localStorage.setItem("data", JSON.stringify([]));
        }

      }
    ).catch((error) => {
      console.log(error)
      this.toggle()
    })

  }

  render() {

    const { redirect } = this.state

    if (redirect) {
      return <Redirect to={{ pathname: `/`, state: this.state }} />
    }

    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="8">
              <CardGroup>
                <Card className="p-4">
                  <CardBody>
                    <Form>
                      <h1>Login</h1>
                      <p className="text-muted">Sign In to your account</p>
                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-user"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" placeholder="Username" autoComplete="username" onKeyPress={(e)=>{this.canSubmit(e)}} onChange={(e) => { this.handleChange(e) }} />
                      </InputGroup>
                      <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="password" placeholder="Password" autoComplete="current-password" onKeyPress={(e)=>{this.canSubmit(e)}} onChange={(e) => { this.handlePassChange(e) }} />
                      </InputGroup>
                      <Row>
                        <Col xs="6">
                          <Row>
                            <Button color="primary" id="login" onClick={() => { this.getData() }} className="px-4">Login</Button>
                          </Row>
                            <br></br>
                          <Row>
                            <FormGroup check>
                              <Label check>
                                <Input onChange={()=>{this.setState({rememberMe: !this.state.rememberMe})}} type="checkbox" />{' '}
                                Remember me
                              </Label>
                            </FormGroup>
                          </Row>
                        </Col>
                        <Col xs="6" className="text-right">
                          <Button color="link" className="px-0">Forgot password?</Button>
                        </Col>
                        <Popover placement="bottom" isOpen={this.state.popoverOpen} target="login" toggle={this.toggle}>
                          <PopoverHeader>Invalid Login</PopoverHeader>
                          <PopoverBody>Please make sure you entered the correct login credentials</PopoverBody>
                        </Popover>
                      </Row>
                    </Form>
                  </CardBody>
                </Card>
                <Card className="text-white bg-primary py-5 d-md-down-none" style={{ width: '44%' }}>
                  <CardBody className="text-center">
                    <div>
                    <img width="1000px !important" src="../../assets/img/round_logo.png" className="img-avatar" alt="sng_round_logo">
                     </img>
                      {/* <h2>Sign up</h2>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                        labore et dolore magna aliqua.</p>
                      <Link to="/register">
                        <Button color="primary" className="mt-3" active tabIndex={-1}>Register Now!</Button>
                      </Link> */}
                    </div>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default Login;