import React, { Component } from 'react';
import { Button, Card, CardBody, Col, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row, Popover, PopoverHeader, PopoverBody, Modal, ModalHeader, ModalFooter, ModalBody } from 'reactstrap';
import rp from 'request-promise';
import md5 from 'md5';
import Cookies from 'universal-cookie';
import ImageUploader from 'react-images-upload';
import { Image} from 'cloudinary-react';


const cookies = new Cookies();

var token = '';


class Settings extends Component {


  constructor(props) {
    super(props)
    this.state = {
      data: [],
      pictures: [],
      modalEXP: false
    }
    this.updateuFname = this.updateuFname.bind(this);
    this.updateuAddr = this.updateuAddr.bind(this);
    this.updateuPhone = this.updateuPhone.bind(this);
    this.updateuPass = this.updateuPass.bind(this);
    this.updateudIcon = this.updateudIcon.bind(this);
    this.oldPass = this.oldPass.bind(this);
    this.toggleError = this.toggleError.bind(this)
    this.getData = this.getData.bind(this)
    this.updateData = this.updateData.bind(this)
    this.toggleEXP = this.toggleEXP.bind(this);
    this.handleChangesEXP = this.handleChangesEXP.bind(this)

  }

  toggleEXP() {
    this.setState({
      modalEXP: !this.state.modal
    });
  }

  handleChangesEXP() {
    this.toggleEXP()
    this.props.history.push('../Login')
    window.location.reload();

  }

  getData() {
    var data = []
    var user_details = {
      uri: "https://sng-heroku-api.herokuapp.com/api/user_details",
      headers: {
        Authorization: 'Bearer ' + token
      },
      method: 'GET',
      json: true
    }
    rp(user_details).then(

      (ud) => {

        data = ud.data
        this.setState({ data: data })
        return ud

      }).catch((error) => {
        if (error.statusCode === 403)
          this.toggleEXP()
      })

    return null
  }

  componentWillMount() {
    if (localStorage.getItem("id_token")) {
      token = localStorage.getItem("id_token")
    }
    
    if (cookies.get('id_token')) {
      token = cookies.get('id_token')
    }

    if (cookies.get('id_token') === undefined && localStorage.getItem('id_token') === null) {
      this.props.history.push('../Login')
    }
    this.setState({ data: [] })
    this.getData()
  }

  toggleError = () => {
    this.setState({
      popoverOpen: !this.state.popoverOpen
    });
  }

  toggleSuccess = () => {
    this.setState({
      spopoverOpen: !this.state.spopoverOpen
    });
  }

  updateuFname(e) {
    this.setState({ uFname: e.target.value })
  }

  updateuAddr(e) {
    this.setState({ uAddr: e.target.value })
  }

  updateuPhone(e) {
    this.setState({ uPhone: e.target.value })
  }

  oldPass(e) {
    this.setState({ oldPass: md5(e.target.value) })
  }

  updateuPass(e) {
    this.setState({ uPass: md5(e.target.value) })
  }

  updateudIcon(image) {

    this.setState({ image: image })
  }


  updateData() {

    const files = this.state.image
    const formData = new FormData()

    files.forEach((file, i) => {
      formData.append(i, file)
    })

    fetch(`http://localhost:4000/image-upload`, {
      method: 'POST',
      body: formData
    })
      .then((res) => {
        res.clone().json().then((image) => {
          this.setState({
            uploading: false,
            udIcon: image[0].public_id
          })
          var datau = JSON.stringify([{ "uPass": this.state.uPass }])
          var dataud = JSON.stringify([{ "uFname": this.state.uFname, "udAddress": this.state.uAddr, "udPhone": this.state.uPhone, "udIcon": this.state.udIcon }])
          var data = JSON.stringify([{ uPass: this.state.oldPass }])
          var loginCheck = {
            uri: "https://sng-heroku-api.herokuapp.com/api/verify_user",
            headers: {
              data: data,
              Authorization: 'Bearer ' + token
            },
            json: true
          }

          rp(loginCheck).then(

            (check) => {
              if (check.data.length === 0) {
                this.toggleError()
              }
              else {
                var settings = {
                  uri: "https://sng-heroku-api.herokuapp.com/api/settings",
                  headers: {
                    data: datau,
                    Authorization: 'Bearer ' + token
                  },
                  method: 'PUT',
                  json: true
                }
                rp(settings).then(

                  () => {

                    var user_details = {
                      uri: "https://sng-heroku-api.herokuapp.com/api/user_details",
                      headers: {
                        data: dataud,
                        Authorization: 'Bearer ' + token
                      },
                      method: 'PUT',
                      json: true
                    }
                    rp(user_details).then(

                      (suc) => {
                        this.toggleSuccess();
                        this.getData();

                        return suc

                      }).catch((error) => {
                        if (error.statusCode === 403)
                          this.toggleEXP()
                      })
                  }).catch((error) => {
                    if (error.statusCode === 403)
                      this.toggleEXP()
                  })
              }
            }).catch((error) => {
              if (error.statusCode === 403)
                this.toggleEXP()
            })

          return null
        })
      })



  }

  render() {
    return (
      <div className="animated fadeIn">
        <Row xl="12" l="12" s="12" >
          <Col>
            <Card>
              <CardBody>

                {this.state.data.map(function (item, key) {
                  return (
                    <Form key={key}>
                      <h1>Settings</h1>
                      <p className="text-muted">Change your personal details</p>
                      <Row>
                        <Col>
                          <Card>
                            <CardBody>
                              <Row>
                                <Col xs="12" lg="12" sm='12'>
                                  <div className="text-center"><Image cloudName="xatzisktv" publicId={item.udIcon} width="200" radius="600" /></div>
                                </Col>
                              </Row>
                              <Row>
                                <Col xs="12" lg="12" sm='12'>
                                  <div className="text-center">
                                    <ImageUploader id={key} singleImage={true} withPreview={false} label={"Upload a new picture for your profile"} withIcon={false} buttonText='Choose image' onChange={this.updateudIcon} imgExtension={['.jpg', '.gif', '.png', '.gif']} maxFileSize={1000000} />
                                  </div>
                                </Col>
                              </Row>
                            </CardBody>
                          </Card>
                        </Col>
                      </Row>
                      <InputGroup  className="mb-3">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText><i className="icon-user "></i></InputGroupText>
                        </InputGroupAddon>
                        <Input id={key} type="text" placeholder={item.uFname} onChange={this.updateuFname} autoComplete="email" />
                      </InputGroup>
                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText><i className="cui-home"></i></InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" placeholder={item.udAddress} onChange={this.updateuAddr} autoComplete="Address" />
                      </InputGroup>
                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText><i className="cui-phone"></i></InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" placeholder={item.udPhone} onChange={this.updateuPhone} autoComplete="Telephone" />
                      </InputGroup>
                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="password" placeholder="Old Password" onChange={this.oldPass} autoComplete="new-password" />
                      </InputGroup>
                      <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="password" placeholder="New password" onChange={this.updateuPass} autoComplete="new-password" />
                      </InputGroup>
                      <Button id='update' color="success" onClick={this.updateData} block>Update</Button>
                      <Popover placement="bottom" isOpen={this.state.popoverOpen} target="update" toggle={this.toggleError}>
                        <PopoverHeader>Wrong Password</PopoverHeader>
                        <PopoverBody>Password missmatch</PopoverBody>
                      </Popover>

                      <Popover placement="bottom" isOpen={this.state.spopoverOpen} target="update" toggle={this.toggleSuccess}>
                        <PopoverHeader>Settings Update</PopoverHeader>
                        <PopoverBody>Settings have been updated successfully</PopoverBody>
                      </Popover>
                    </Form>

                  )
                }.bind(this))}

              </CardBody>
            </Card>
          </Col>
        </Row>
        <Modal className="modal-info" isOpen={this.state.modalEXP} toggle={this.toggleEXP}>
          <ModalHeader toggle={this.toggleEXP}>Session Expired</ModalHeader>
          <ModalBody>
            Your session has expired. Please log in to continue.
          </ModalBody>
          <ModalFooter>
            <Button color="info" onClick={this.handleChangesEXP}>Yes</Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

export default Settings;

