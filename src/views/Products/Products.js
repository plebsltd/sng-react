import React, { Component } from 'react';
import { Card, CardBody, CardTitle, CardHeader, Col, Row, Table, Button, Modal, ModalHeader, ModalBody, ModalFooter, Input, Pagination, PaginationItem, PaginationLink } from 'reactstrap';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import rp from 'request-promise';
import { ToastContainer, toast } from 'react-toastify';
// eslint-disable-next-line
import toastifyFont from 'react-toastify/dist/ReactToastify.css';
import Cookies from 'universal-cookie';
import { Image} from 'cloudinary-react';

const cookies = new Cookies();

var data = []
var userID = 0
var orderID = 0
var token = ''


localStorage.__proto__.addItem = function (item) {

  localStorage.setItem('data', item)

  var event = new Event('itemInserted');
  window.dispatchEvent(event);

}

class Products extends Component {

  constructor(props) {
    super(props)
    this.state = {
      data: null,
      sort: '',
      distributors: [],
      dist: "ALL",
      prod_count: [],
      current_page: 1,
      quantity: '1',
      selected_dist: null,
      modalEXP: false

    }
    this.compareBy.bind(this);
    //this.sortBy.bind(this);
    this.thisrch = this.thisrch.bind(this);
    this.updateQuantity = this.updateQuantity.bind(this)
    this.removeFilter = this.removeFilter.bind(this)
    this.toggleEXP = this.toggleEXP.bind(this);
    this.handleChangesEXP = this.handleChangesEXP.bind(this)
    
    if (localStorage.getItem("data") === null) {
      localStorage.setItem("data", JSON.stringify([]))
    }
    else {
      this.basket = JSON.parse(localStorage.getItem("data"))
    }

  }

  toggleEXP() {
    this.setState({
      modalEXP: !this.state.modal
    });
  }

  handleChangesEXP() {
    this.toggleEXP()
    this.props.history.push('../Login')
    window.location.reload();

  }
  compareBy(key) {
    return function (a, b) {
      if (a[key] < b[key]) return -1;
      if (a[key] > b[key]) return 1;
      return 0;
    };
  }




  // sortBy(key) {
  //   let arrayCopy = [...this.state.data];

  //   if (this.state.sort === '' || this.state.sort === 'desc') {
  //     this.setState({ sort: 'asc' })
  //     arrayCopy.sort(this.compareBy(key));
  //   }

  //   if (this.state.sort === 'asc') {
  //     this.setState({ sort: 'desc' })
  //     arrayCopy.reverse(this.compareBy(key));
  //   }

  //   this.setState({ data: arrayCopy });
  // }

  sendData = (item) => {

    localStorage.setItem(item)
  }


  getData = () => {

    if (Object.keys(this.props.match.params).length === 0 && this.props.match.params.constructor === Object) {
      var data = []
      var uproducts = {
        uri: "https://sng-heroku-api.herokuapp.com/api/products",
        headers: {
          Authorization: 'Bearer ' + token
        },
        json: true
      }
      rp(uproducts).then(

        (prod) => {

          data = prod.data
          data.forEach((i) => {
            i.quantity = 1
          })
          this.props.location.state = { data: data };
          this.setState({ data: data, all_data: data })

          return prod

        }
      ).catch((error) => {
        if(error.statusCode===403)
          this.toggleEXP()
      }).then(

        () => {
          var distributors = {
            uri: "https://sng-heroku-api.herokuapp.com/api/distributors",
            headers: {
              Authorization: 'Bearer ' + token
            },
            json: true
          }
          rp(distributors).then(

            (dist) => {

              this.setState({ distributors: dist.data })

              return dist

            }).catch((error) => {
              console.log(error)
              this.toggleEXP()
            }).then(

              () => {
                var prod_count = {
                  uri: "https://sng-heroku-api.herokuapp.com/api/custom",
                  headers: {
                    data: "SELECT COUNT(*) as cp FROM products",
                    Authorization: 'Bearer ' + token
                  },
                  json: true
                }
                rp(prod_count).then(

                  (count) => {

                    this.setState({ prod_count: count.data[0].cp })

                    return count;

                  }, (error) => {
                    console.log(error)
                    return error;
                  }).catch((error) => {
                    if(error.statusCode===403)
                      this.toggleEXP()
                  })
                  return null
              }).catch((error) => {
                if(error.statusCode===403)
                  this.toggleEXP()
              })
              return null
        }).catch((error) => {
          if(error.statusCode===403)
            this.toggleEXP()
        })
      return true
    }

    else {

      var products = {
        uri: "https://sng-heroku-api.herokuapp.com/api/products",
        headers: {
          Authorization: 'Bearer ' + token
        },
        json: true
      }
      rp(products).then(

        (prod) => {

          var all_data = prod.data
          all_data.forEach((i) => {
            i.quantity = 1
          })
          var did = this.props.match.params.did
          var data = all_data.filter((prod) => {
            return prod.pDID === parseInt(did)
          })
          this.props.location.state = { data: data, all_data: all_data };
          this.setState({ data: data, dist: data[0].uFname, selected_dist: data[0], distID: data[0].udID, all_data: all_data })

          return prod

        }).catch((error) => {
          if(error.statusCode===403)
            this.toggleEXP()
        }).then(

          () => {

            var distributors = {
              uri: "https://sng-heroku-api.herokuapp.com/api/distributors",
              headers: {
                Authorization: 'Bearer ' + token
              },
              json: true
            }
            rp(distributors).then(

              (dist) => {
                this.setState({ distributors: dist.data })

                return dist

              }).catch((error) => {
                if(error.statusCode===403)
                  this.toggleEXP()
              }).then(

                () => {

                  var prod_count = {
                    uri: "https://sng-heroku-api.herokuapp.com/api/custom",
                    headers: {
                      data: "SELECT COUNT(*) as cp FROM products WHERE pDID='" + this.props.match.params.did + "'",
                      Authorization: 'Bearer ' + token
                    },
                    json: true
                  }
                  rp(prod_count).then(

                    (count) => {

                      this.setState({ prod_count: count.data[0].cp })

                      return count

                    }).catch((error) => {
                      if(error.statusCode===403)
                        this.toggleEXP()
                    })
                }).catch((error) => {
                  if(error.statusCode===403)
                    this.toggleEXP()
                })
                return null
          }).catch((error) => {
            if(error.statusCode===403)
              this.toggleEXP()
          })
      return true


    }




  }
  updateQuantity(event, item) {
    let arrayCopy = [...this.state.data]
    arrayCopy.forEach((i) => {
      if (i.pID === item.pID) {
        if (event.target.value > 999) {
          i.quantity = 999
        }
        else if (event.target.value < 1) {
          i.quantity = 1
        }
        else {
          i.quantity = parseInt(event.target.value)
        }

        this.setState({ data: arrayCopy });
      }
    })
  }

  addProduct(item) {
    this.basket = JSON.parse(localStorage.getItem("data"))

    var prod_found = false
    for (var elem of this.basket) {
      if (item.pID === elem.pID) {
        prod_found = true
        elem.quantity += item.quantity
        break
      }
    }

    if (!prod_found) {
      this.basket.push(item)
    }

    localStorage.addItem(JSON.stringify(this.basket))

    toast.info(item.pName + " has been added to your basket!", {
      position: toast.POSITION.BOTTOM_CENTER,
      autoClose: 3000
    });
  }

  removeFilter() {
    if (this.props.history.location.pathname !== "/Products")
      this.props.history.replace('../Products')
  }

  componentWillMount() {
    if (localStorage.getItem("id_token")) {
      token = localStorage.getItem("id_token")
    }
    
    if (cookies.get('id_token')) {
      token = cookies.get('id_token')
    }

    if (cookies.get('id_token') === undefined && localStorage.getItem('id_token') === null) {
      this.props.history.push('../Login')
    }

    this.setState({ data: data, userID: userID, orderID: orderID, token: token })
    this.getData();


  }

  thisrch(e) {
    this.setState({ current_page: 1 })
    var string = e.target.value.toLowerCase().trim()
    var splitString = string.split(" ")
    var splitted = ""
    var data = []
    if(splitString.length === 1){
      data = this.props.location.state.data.filter((prod) => {
        return prod.pName.toLowerCase().match(splitString)
        //return value.test(prod.pName.toLowerCase())
      })
    }
    else if(splitString.length > 1){
      splitted = "(?=.*" + splitString.join(")(?=.*") + ")"
      var value = new RegExp(splitted)
      data = this.props.location.state.data.filter((prod) => {
        return prod.pName.toLowerCase().match(value)
        //return value.test(prod.pName.toLowerCase())
    })
  }
    this.setState({ data: data })
  }

  handleChange = e => {

    this.setState({ [e.target.name]: e.target.value });

    var data = this.state.all_data.filter((prod) => {

      return prod.uFname.includes(e.target.value)

    })

    var selectedDist = this.state.distributors.filter((dist) => {
      return dist.uFname.includes(e.target.value)
    })
    this.setState({ data: data, selected_dist: selectedDist[0] })
    this.props.history.push(`../Products/${selectedDist[0].udID}`, { data: data })


  };

  render() {
    if (localStorage.getItem("userType") === '1') {
      this.props.history.replace('/404')
      return null
    }
    else {


      return (

        <div className="animated fadeIn">
          <ToastContainer />
          {
            this.state.selected_dist != null ? (
              <Row>
                <Col>
                  <Card>
                    <CardBody>
                      <Row>
                        {/* <Col xs="12" lg="1" sm='1'> */}
                        <Col md="auto">
                          <div className="text-center"><Image cloudName="xatzisktv" publicId={this.state.selected_dist.udIcon} width="80" radius="150" /></div>
                        </Col>
                        {/* <Col xs="12" lg="11" sm='11'> */}
                        <Col md="auto">
                          <CardTitle className="mb-0">{this.state.selected_dist.uFname}</CardTitle>
                          <div className="small text-muted"><i className="fa fa-envelope"></i>{' Email: '}<a href={"mailto:" + this.state.selected_dist.udEmail}>{this.state.selected_dist.udEmail}</a></div>
                          <div className="small text-muted"><i className="fa fa-phone"></i>{' Phone: ' + this.state.selected_dist.udPhone}</div>
                          <div className="small text-muted"><i className="fa fa-map-pin"></i>{' Address: ' + this.state.selected_dist.udAddress}</div>
                        </Col>
                      </Row>
                    </CardBody>
                  </Card>
                </Col>
              </Row>
            ) : (false)
          }
          <Row>
            <Col xs="12" lg="12">
              <Card>
                <CardHeader>

                  <i className="fa fa-align-justify"></i> Products
                <Button color="danger" size="sm" className="float-right" onClick={this.removeFilter}><i className="fa fa-remove "></i></Button>
                  <div className="col-sm-2 float-right" style={{ 'width': '10px' }}></div>
                  <Select style={{ fontSize: '90%' }} onChange={(e) => { this.handleChange(e) }} value={this.state.dist} className="col-sm-2 float-right" inputProps={{ name: 'dist', id: 'dist' }}>
                    {this.state.distributors.map(function (item, key) {
                      return (
                        <MenuItem style={{ fontSize: '90%' }} key={key} value={item.uFname}>{item.uFname}</MenuItem>
                      )
                    })}
                  </Select>
                  <div className="col-sm-2 float-right" style={{ 'width': '10px' }}></div>

                  <Input className="col-sm-2 float-right" placeholder="Search" onChange={(e) => { this.thisrch(e) }} />



                </CardHeader>
                <CardBody>
                  <Table responsive className="table table-hover">
                    <thead>
                      <tr>
                         
                        <th scope="col"  style={{ cursor: 'pointer' }}>Name</th>
                        <th scope="col"  style={{ cursor: 'pointer' }}>Price</th>
                        <th scope="col"  style={{ cursor: 'pointer' }}>VAT</th>
                        <th scope='col'  style={{ cursor: 'pointer' }}>Distributor</th>
                        <th scope='col'  style={{ cursor: 'pointer' }}>Quantity</th>
                        <th scope='col'></th>

                      </tr>
                    </thead>
                    <tbody>
                      {
                        this.state.data.map(function (item, key) {
                          if (key < this.state.current_page * 10 && key >= this.state.current_page * 10 - 10) {

                            return (
                              <tr key={key} className={this.status}>
                                <td><div>{item.pName}</div><div className="small text-muted"><span>Description</span> : {item.pDesc}</div></td>

                                <td>{new Intl.NumberFormat('en-GB', {
                                  style: 'currency',
                                  currency: 'EUR',
                                  minimumFractionDigits: 2,
                                  maximumFractionDigits: 2
                                }).format(item.pPrice)}</td>
                                <td>{Math.floor((item.pVAT) * 100) + "%"}</td>
                                <td><a href={'./#/Products/' + item.pDID}>{item.uFname}</a></td>
                                <td><input disabled={item.pStock !== 1} className="form-control" id="input-normal" type="number" value={item.quantity} onChange={(e) => { this.updateQuantity(e, item) }} name="input-normal" min={1} max={999} placeholder="Quantity" /></td>

                                <td>
                                  {item.pStock === 1 ? (
                                    <Button onClick={() => { this.addProduct(item) }} className={"btn btn-ghost-primary"}>Add to basket <i className="icon-basket-loaded icons font-xl"></i></Button>
                                  ) : (
                                      <Button className={"btn disabled btn-ghost-disabled"}>Out of Stock <i className="icon-ban icons font-xl"></i></Button>
                                    )
                                  }
                                </td>
                              </tr>
                            )
                          }
                        }.bind(this))
                      }

                    </tbody>
                  </Table>

                  <Pagination>
                    <PaginationItem>
                      <PaginationLink tag="button" onClick={() => { this.setState({ current_page: 1 }) }}>First</PaginationLink>
                    </PaginationItem>
                    <PaginationItem>
                      <PaginationLink previous tag="button" onClick={() => { if (this.state.current_page * 10 - 10 > 0) { this.setState({ current_page: (this.state.current_page - 1) }) } }}></PaginationLink>
                    </PaginationItem>
                    {
                      this.state.data.map(function (item, key) {
                        key += 1
                        if (key % 10 === 1) {
                          let temp_page = Math.trunc((key / 10) + 1)
                          let last_page = Math.trunc((this.state.data.length / 10) + 1)
                          // Dot system
                          if (last_page > 10) {
                            // Inner current page
                            if (this.state.current_page >= 6 && last_page - this.state.current_page >= 6) {
                              if (temp_page < 3) {
                                return (
                                  <PaginationItem active={this.state.current_page === temp_page}>
                                    <PaginationLink onClick={() => { this.setState({ current_page: temp_page }) }} tag="button">{temp_page}</PaginationLink>
                                  </PaginationItem>
                                )
                              }
                              else if (temp_page === 3) {
                                return (
                                  <PaginationItem disabled>
                                    <PaginationLink tag="button">...</PaginationLink>
                                  </PaginationItem>
                                )
                              } else if (temp_page - this.state.current_page >= -2 && temp_page - this.state.current_page <= 2) {
                                return (
                                  <PaginationItem active={this.state.current_page === temp_page}>
                                    <PaginationLink onClick={() => { this.setState({ current_page: temp_page }) }} tag="button">{temp_page}</PaginationLink>
                                  </PaginationItem>
                                )
                              }

                              if (last_page - temp_page < 2) {
                                return (
                                  <PaginationItem active={this.state.current_page === temp_page}>
                                    <PaginationLink onClick={() => { this.setState({ current_page: temp_page }) }} tag="button">{temp_page}</PaginationLink>
                                  </PaginationItem>
                                )
                              }
                              else if (last_page - temp_page === 2) {
                                return (
                                  <PaginationItem disabled>
                                    <PaginationLink tag="button">...</PaginationLink>
                                  </PaginationItem>
                                )
                              }

                            }
                            // lower current page
                            else if (this.state.current_page <= 6) {
                              if (last_page - temp_page < 2) {
                                return (
                                  <PaginationItem key={key} active={this.state.current_page === temp_page}>
                                    <PaginationLink onClick={() => { this.setState({ current_page: temp_page }) }} tag="button">{temp_page}</PaginationLink>
                                  </PaginationItem>
                                )
                              }
                              else if (last_page - temp_page === 2) {
                                return (
                                  <PaginationItem key={key} disabled>
                                    <PaginationLink tag="button">...</PaginationLink>
                                  </PaginationItem>
                                )
                              } else if (temp_page <= 6) {
                                return (
                                  <PaginationItem key={key} active={this.state.current_page === temp_page}>
                                    <PaginationLink onClick={() => { this.setState({ current_page: temp_page }) }} tag="button">{temp_page}</PaginationLink>
                                  </PaginationItem>
                                )
                              }
                            }
                            // upper current page
                            else if (last_page - this.state.current_page <= 6) {
                              if (temp_page < 3) {
                                return (
                                  <PaginationItem active={this.state.current_page === temp_page}>
                                    <PaginationLink onClick={() => { this.setState({ current_page: temp_page }) }} tag="button">{temp_page}</PaginationLink>
                                  </PaginationItem>
                                )
                              }
                              else if (temp_page === 3) {
                                return (
                                  <PaginationItem disabled>
                                    <PaginationLink tag="button">...</PaginationLink>
                                  </PaginationItem>
                                )
                              } else if (last_page - temp_page <= 6) {
                                return (
                                  <PaginationItem active={this.state.current_page === temp_page}>
                                    <PaginationLink onClick={() => { this.setState({ current_page: temp_page }) }} tag="button">{temp_page}</PaginationLink>
                                  </PaginationItem>
                                )
                              }
                            }
                          } else {
                            return (
                              <PaginationItem active={this.state.current_page === temp_page}>
                                <PaginationLink onClick={() => { this.setState({ current_page: temp_page }) }} tag="button">{temp_page}</PaginationLink>
                              </PaginationItem>
                            )
                          }
                        }
                      }.bind(this)
                      )
                    }

                    <PaginationItem>
                      <PaginationLink next tag="button" onClick={() => { if (this.state.current_page * 10 + 1 < this.state.data.length) { this.setState({ current_page: (this.state.current_page + 1) }) } }}></PaginationLink>
                    </PaginationItem>

                    <PaginationItem>
                      <PaginationLink tag="button" onClick={() => { if (this.state.data.length % 10 > 0) { this.setState({ current_page: Math.trunc((this.state.data.length / 10) + 1) }) } else { this.setState({ current_page: this.state.data.length / 10 }) } }}>Last</PaginationLink>
                    </PaginationItem>
                  </Pagination>

                  <Modal isOpen={this.state.modal} toggle={this.toggle}>
                    <ModalHeader toggle={this.toggle}>Modal title</ModalHeader>
                    <ModalBody>
                      Are you sure you want to edit this item?
                  </ModalBody>
                    <ModalFooter>
                      <Button color="primary" onClick={this.toggle}>Do Something</Button>{' '}
                      <Button color="secondary" onClick={this.toggle}>Cancel</Button>
                    </ModalFooter>
                  </Modal>
                </CardBody>
              </Card>
            </Col>
          </Row>
          <Modal className="modal-info" isOpen={this.state.modalEXP} toggle={this.toggleEXP}>
            <ModalHeader toggle={this.toggleEXP}>Cancel Order</ModalHeader>
            <ModalBody>
              Your session has expired. Please log in to continue. 
            </ModalBody>
            <ModalFooter>
              <Button color="info" onClick={this.handleChangesEXP}>Yes</Button>
            </ModalFooter>
          </Modal>
        </div>

      );
    }
  }
}

export default Products;